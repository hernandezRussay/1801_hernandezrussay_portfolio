﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class Program
    {
        static void Main(string[] args)
        {
            /* 
            Russay Hernandez
            1801
            Project & Portfolio 1
            Synopsis: Default Program.cs
            */

            /* Put each challenge into the menu class and only instantiate from the menu class
            
            //Challenge 1
            //Creating/instantiating a new object that completes the first challenge and calling it MySwapInfo
            SwapInfo MySwapInfo = new SwapInfo();

            //Challenge 2
            //Instantiating a new object that completes the 2nd challenge and calling it MyBackwards
            Backwards MyBackwards = new Backwards();

            //Challenge 3
            //Instantiating a new object that completes the 3rd challenge and naming it MyAgeConvert
            AgeConvert MyAgeConvert = new AgeConvert();

            //Challenge 4
            //Instantiating a new object that completes the 4th challenge and naming it MyTempConvert
            TempConvert MyTempConvert = new TempConvert();
            */


            //Challenge 5
            //Instantiating a new object that completes the 5th challenge and naming it MyMenu
            Menu MyMenu = new Menu();
        }
    }
}
