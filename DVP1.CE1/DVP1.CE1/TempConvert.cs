﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    public class TempConvert
    {
        /*
            Russay Hernandez
            1801
            Project & Portfolio 1
            Synopsis: Challenge 4: number exercise: TempConvert
            */

        public TempConvert()
        {
            //Prompting user for a temperature in Fahrenheit
            Console.WriteLine("Please enter a temperature in Farenheit: ");
            string temp1String = Console.ReadLine();
            double temp1;
            //Validating that it's a number
            while (!(double.TryParse(temp1String, out temp1)))
            {
                Console.WriteLine("You entered something other than a number. Please enter a temperature in Farenheit: ");
                temp1String = Console.ReadLine();
            }
            //Displaying the user input to the console
            Console.WriteLine("You entered {0} degrees Fahrenheit.", temp1); 

            //Calling the method convertFahrenheit with the user input as a parameter, saving it and outputting it to console
            double convertedTemp1 = convertFahrenheit(temp1);
            Console.WriteLine("{0} degrees Fahrenheit converted is {1} degrees celcius.", temp1, convertedTemp1);


            //Prompting user for a temperature in Celcius
            Console.WriteLine("Please enter a temperature in Celcius: ");
            string temp2String = Console.ReadLine();
            double temp2;
            //Validating that it's a number
            while (!(double.TryParse(temp2String, out temp2)))
            {
                Console.WriteLine("You entered something other than a number. Please enter a temperature in Celcius: ");
                temp2String = Console.ReadLine();
            }
            //Displaying the user input to the console
            Console.WriteLine("You entered {0} degrees Celcius.", temp2);

            //Calling the method convertCelcius with the user input as a parameter, saving it and outputting it to console
            double convertedTemp2 = convertCelcius(temp2);
            Console.WriteLine("{0} degrees Celcius converted is {1} degrees Fahrenheit.", temp2, convertedTemp2);
        }

        //Create a new method that converts fahrenheit to celcius
        public static double convertFahrenheit(double _fahrenheit)
        {
            double celcius = (_fahrenheit - 32) * 5 / 9;
            return celcius;
        }

        //Create a new method that converts celcius to Fahrenheit
        public static double convertCelcius(double _celcius)
        {
            double Fahrenheit = (_celcius * 9 / 5) + 32;
            return Fahrenheit;
        }
    }
}
