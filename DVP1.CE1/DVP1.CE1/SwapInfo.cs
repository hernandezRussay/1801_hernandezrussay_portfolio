﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    public class SwapInfo
    {
        /*
            Russay Hernandez
            1801
            Project & Portfolio 1
            Synopsis: Challenge 1: String Exercise: SwapInfo
            */
        //Create a method that prompts the user for info, validates it, uses another method to reverse the info, and prints it to the console
        public SwapInfo()  
        {
            //asking the user for their first name
            Console.WriteLine("Please enter your first name: ");
            string firstName = Console.ReadLine();
            //validating the response with a while loop to make sure they did not leave it blank
            while (string.IsNullOrWhiteSpace(firstName))
            {
                Console.WriteLine("Please do not leave this blank. Enter your first name: ");
                firstName = Console.ReadLine();
            }
            //Doing the same thing with their last name
            Console.WriteLine("Please enter your last name: ");
            string lastName = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(lastName))
            {
                Console.WriteLine("Please do not leave this blank. Enter your last name: ");
                lastName = Console.ReadLine();
            }
            //Showing the user what they entered
            Console.WriteLine("You entered the name {0} {1}.", firstName, lastName);

            //Calling the method below to switch the info and storing it in the variable newOrder
            string newOrder = switchNames(firstName, lastName);

            //Outputting the reversed info
            Console.WriteLine("This is your name in reverse order: {0}.", newOrder);

        }

        //Creating a method to swap the first and last names
        public static string switchNames(string _firstname, string _lastname)
        {
            string a = _firstname;
            _firstname = _lastname;
            _lastname = a;
            //returning the swapped first and last names together
            return _firstname +" "+ _lastname;
        }
    }
}
