﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    public class AgeConvert
    {
        /*
            Russay Hernandez
            1801
            Project & Portfolio 1
            Synopsis: Challenge 3: number exercise: AgeConvert
            */

        public AgeConvert()
        {
            //Prompting user for their name
            Console.WriteLine("Please enter your name: ");
            string userName = Console.ReadLine();
            //Validating input that it isn't blank
            while (string.IsNullOrWhiteSpace(userName))
            {
                Console.WriteLine("Please do not leave blank. Enter your name: ");
                userName = Console.ReadLine();
            }
            //Prompting user for their age
            Console.WriteLine("Please enter your age: ");
            string userAgeString = Console.ReadLine();
            int userAge;
            //Validating that it's a number
            while (!(int.TryParse(userAgeString, out userAge)))
            {
                Console.WriteLine("You entered something other than a number. Please enter your age: ");
                userAgeString = Console.ReadLine();
            }
            //Displaying the user input to the console
            Console.WriteLine("Your name is {0} and you are {1} years old.", userName, userAge);

            //returning value from convertDays and displaying it on the console
            int days = convertDays(userAge);
            Console.WriteLine("Your name is {0} and you have been alive {1} days.", userName, days);

            //getting hours from days by multiplying it by 24 and displaying it on the console
            int hours = days * 24;
            Console.WriteLine("Your name is {0} and you have been alive {1} hours.", userName, hours);

            //getting miuntes from hours by multiplying it by 60 and displaying it on the console
            int minutes = hours * 60;
            Console.WriteLine("Your name is {0} and you have been alive {1} minutes.", userName, minutes);

            //getting seconds from minutes by multiplying it by 60 and displaying it on the console
            int seconds = minutes * 60;
            Console.WriteLine("Your name is {0} and you have been alive {1} seconds.", userName, seconds);
        }
        //Created new method to convert age to days alive
        public static int convertDays(int _userAge)
        {
            int days = _userAge * 365;
            return days;
        }
    }
}
