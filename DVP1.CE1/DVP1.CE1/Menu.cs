﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    public class Menu
    {
        /*
            Russay Hernandez
            1801
            Project & Portfolio 1
            Synopsis: Challenge 4: conditional exercise: Menu
            */

        public Menu()
        {
            //Prompt the user to choose a coding challenge to run from a menu
            Console.WriteLine("Please choose a coding challenge to run from the following menu.");
            Console.WriteLine("1 - SwapInfo");
            Console.WriteLine("2 - Backwards");
            Console.WriteLine("3 - AgeConvert");
            Console.WriteLine("4 - TempConvert");
            Console.WriteLine("Type in the number of the challenge you want to run: ");
            string choiceString = Console.ReadLine();
            //validate that the user inputted a string
            int choice;
            while (!(int.TryParse(choiceString, out choice)))
            {
                Console.WriteLine("You entered something other than a number. Please enter either 1, 2, 3 or 4: ");
                choiceString = Console.ReadLine();
            }
            //validate that the user typed in a number that is either 1, 2, 3 or 4
            if (!((choice == 1) || (choice == 2) || (choice == 3) || (choice == 4)))
            {
                Console.WriteLine("You entered something other that 1, 2, 3 or 4. Please enter one of these choices: ");
                choiceString = Console.ReadLine();
            }
            while (!(int.TryParse(choiceString, out choice)))
            {
                Console.WriteLine("You entered something other than a number. Please enter either 1, 2, 3 or 4: ");
                choiceString = Console.ReadLine();
            }

            //Creating an object from the class the user choses
            if (choice == 1)
            {

                SwapInfo MySwapInfo = new SwapInfo();
                //Prompting the user to go back to the menu by entering "Y"
                Console.WriteLine("\r\nWould you like to return to the menu? Enter Y or N: ");
                string toReturn = Console.ReadLine();
                if (toReturn == "Y" || toReturn == "y")
                {
                    Console.WriteLine("\r\n");
                    Menu MyMenu = new Menu();
                }
            }
            else if (choice == 2)
            {
                Backwards MyBackwards = new Backwards();
                Console.WriteLine("\r\nWould you like to return to the menu? Enter Y or N: ");
                string toReturn = Console.ReadLine();
                if (toReturn == "Y")
                {
                    Console.WriteLine("\r\n");
                    Menu MyMenu = new Menu();
                }
            }
            else if (choice == 3)
            {
                AgeConvert MyAgeConvert = new AgeConvert();
                Console.WriteLine("\r\nWould you like to return to the menu? Enter Y or N: ");
                string toReturn = Console.ReadLine();
                if (toReturn == "Y")
                {
                    Console.WriteLine("\r\n");
                    Menu MyMenu = new Menu();
                }
            }
            else if (choice == 4)
            {
                TempConvert MyTempConvert = new TempConvert();
                Console.WriteLine("\r\nWould you like to return to the menu? Enter Y or N: ");
                string toReturn = Console.ReadLine();
                if (toReturn == "Y")
                {
                    Console.WriteLine("\r\n");
                    Menu MyMenu = new Menu();
                }
            }
        }
    }
}
