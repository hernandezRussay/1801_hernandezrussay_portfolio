﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    public class Backwards
    {
        /*
            Russay Hernandez
            1801
            Project & Portfolio 1
            Synopsis: Challenge 2: String Exercise: Backwards
            */

        public Backwards()
        {
            Console.WriteLine("Please enter a sentence at least 6 words long: ");
            string originalSentence = Console.ReadLine();
            //Validating to make sure it is not left blank
            while (string.IsNullOrWhiteSpace(originalSentence))
            {
                Console.WriteLine("Please do not leave this blank. Enter a sentence at least 6 words long: ");
                originalSentence = Console.ReadLine();
            }
            //validating to make sure the sentence is at least 6 words long by counting the spaces           
            int numberOfSpaces = 0;
            for (int count = originalSentence.Length - 1; count >= 0; count--)
            {
                if (originalSentence[count] == ' ')
                {
                    numberOfSpaces += 1;
                }
            }
            while (numberOfSpaces < 5)
            {
                Console.WriteLine("You didn't enter at least 6 words. Type in a sentence at least 6 words long: ");
                originalSentence = Console.ReadLine();
                for (int count = originalSentence.Length - 1; count >= 0; count--) 
                {
                    if (originalSentence[count] == ' ')
                    {
                        numberOfSpaces += 1;
                    }
                }
            }
            

            //calling the method below and saving the returned value as reversed
            string reversed = reversedSentence(originalSentence);

            Console.WriteLine("Here is the original sentence: " + originalSentence);
            Console.WriteLine("Here is the reversed sentence: " + reversed);
        }

        //creating a new method to reverse the sentence
        public static string reversedSentence(string originalSentence)
        {
            //declare a new variable to store the new reversed sentence
            string reversedNew = "";
            //using a for loop to go through each character backwards (from end to beginnning) and storing it in reversedNew in the new order
            for (int count = originalSentence.Length -1; count >= 0; count--)
            {

                reversedNew += originalSentence[count];
            }
            //returning the reversed sentence
            return reversedNew;
        }
    }
}
