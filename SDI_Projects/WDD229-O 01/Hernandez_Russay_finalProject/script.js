
  class Game {
  	wins fields
  	constructor(username){
  		this.username = prompt("Enter your name :") || username;
  		this.results = [];
  		this.wins=0;
  	}
  
  	addResult(result){
  		this.results.push(result)
  	}
  
  	showResults(){
  		this.results.forEach( function(element) {
  			console.log(element);
  		});
  	}
  	
  	addWin(){
  		this.wins++;
  	}
  
  	showWins(){
  		console.log(this.username+" wins "+ this.wins+" times!");
  	}
  	//main method that start game cycle
  	play(){
  		let userPick = prompt("Do you choose rock, paper or scissors?");
        if (! userPick) {
            console.log(this.username+", you cheated!");
        } else {
            console.log(this.username+":" + " " + userPick);
        }
        // Computer pick
        let computerPick = Math.random();
        if (computerPick < 0.34) {
            computerPick = "rock";
        } else if(computerPick <= 0.67) {
            computerPick = "paper";
        } else {
            computerPick = "scissors";
        }
        console.log("Computer:" + " " + computerPick);
        let checkWin = (firstChoice,secondChoice)=> {
            if (firstChoice === secondChoice) {
                return "It's a tie!";
            }
            if (firstChoice === "rock") {
                if (secondChoice === "scissors") {
                    this.wins++;
                    return "You win!";
                } else {
                    return "You lose! Try again.";
                }
            }
            if (firstChoice === "paper") {
                if (secondChoice === "rock") {
                    this.wins++;
                    return "You win!";
                } else {
                    return "You lose! Try again.";
                }
            }
            if (firstChoice === "scissors") {
                if (secondChoice === "rock") {
                    return "You lose! Try again.";
                } else {
                    this.wins++;
                    return "You win!";
                }
            }
        };
        console.log(checkWin(userPick,computerPick));
  	}
  }

        let game = new Game();
        let play = true;
        let gameNumber = 0,compWins = 0;
        //starting game loop
        while(play){
        	game.play();
        	let result = {gameNumber:gameNumber++, wins:{computerWins:compWins++}};
        	game.addResult(result);
        	//check if user wanna play more 
        	play = confirm("You wanna play more?");
    	}

        game.showWins();
        game.showResults();
        
