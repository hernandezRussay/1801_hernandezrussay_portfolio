﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hernandez_Russay_Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Russay Hernandez
            December 27th, 2017
            Methods Assignment
            */

            // Problem # 1: spaceCadet();

            Console.WriteLine("Please enter the weight of the person: "); 
            string weight = Console.ReadLine();
            double weight2;
            while (!double.TryParse(weight, out weight2))        //validated the user to ensure i get numbers and not letters
            {
                Console.WriteLine("You entered something other than a number. Please enter the weight of the person: ");
                weight = Console.ReadLine();
            }

            double moonWeight = SpaceCadet(weight2);               //returned the calculated moon weight from the SpaceCadet() function

            Console.WriteLine("On Earth you weigh {0} lbs, but on the moon you would only weight {1} lbs.", weight2, Math.Round(moonWeight, 2));  //printed out moon weight to the console, and rounding to 2 decimal places

            /*
            Data Sets To Test:
            Astronaut's Weight: 160 lbs.
            Result: "On Earth you weigh 160 lbs, but on the moon you would only weight 26.67 lbs."
            Astronaut's Weight: 210 lbs.
            Result: "On Earth you weigh 210 lbs, but on the moon you would only weight 35 lbs."
            Astronaut's Weight: One Hundred Pounds
            Result: Re-prompt for number value.
            Astronaut's Weight: 100 lbs
            Result: "On Earth you weigh 100 lbs, but on the moon you would only weight 16.67 lbs."
            */


            //  discountCalculator();
            Console.WriteLine("Cost of first item: ");
            string firstItem = Console.ReadLine();
            decimal firstItem1;
            while (!decimal.TryParse(firstItem, out firstItem1))                //validated the user to ensure i get numbers and not letters
            {
                Console.WriteLine("Please re enter the cost of the first item");     //prompted the user to re enter information should he type a letter instead of a number
                firstItem = Console.ReadLine();
            }

            Console.WriteLine("Cost of second item: ");
            string secondItem = Console.ReadLine();
            decimal secondItem1;
            while (!decimal.TryParse(secondItem, out secondItem1))                //validated the user to ensure i get numbers and not letters
            {
                Console.WriteLine("Please re enter the cost of the second item");     //prompted the user to re enter information should he type a letter instead of a number
                secondItem = Console.ReadLine();
            }
            Console.WriteLine("Dicount %: ");
            string discountPercent = Console.ReadLine();
            decimal discountPercent1;
            while (!decimal.TryParse(discountPercent, out discountPercent1))                //validated the user to ensure i get numbers and not letters
            {
                Console.WriteLine("Please re enter the discount");     //prompted the user to re enter information should he type a letter instead of a number
                discountPercent = Console.ReadLine();
            }

            decimal bill = DiscountCalculator(firstItem1, secondItem1, discountPercent1);            //returned the total of the bill from the DicsountCalculator() function
            Console.WriteLine("With a {0}% discount your total is ${1}", discountPercent1, Math.Round(bill, 2));     //printed out the result to the console

            /*
            Data Sets To Test:
            Cost 1 – 10.00 Cost 2- 15.50 Discount % - 20
            Results - "With a 20% discount your total is $20.40"
            Cost 1 – 20.25 Cost 2- 37.75 Discount % - 10
            Results - "With a 10% discount your total is $52.20"
            Cost 1 – 45.00 Cost 2- 20.00 Discount % - 5
            Results - "With a 5% discount your total is $61.75"

            */



            int[] oldArray = new int[] { 45, 33, 42, 85, 35 };              //defined original array with integers

            int[] newArray = DoubleFun(oldArray);             //returned new array from DoubleFun() function
            //printed out result to the console
            Console.WriteLine("Your original array was [{0}, {1}, {2}, {3}, {4}] and now it is doubled it is [{5}, {6}, {7}, {8}, {9}]", oldArray[0], oldArray[1], oldArray[2], oldArray[3], oldArray[4], newArray[0], newArray[1], newArray[2], newArray[3], newArray[4]);


            /*
            Data Sets To Test:
            Initial Array- [1, 2, 5, 6, 9]
            Results - Your original array was [1, 2, 5, 6, 9] and now it is doubled it is [2, 4, 10, 12, 18]
            Initial Array- [12, 3, 8, 20, 7]
            Results - Your original array was [12, 3, 8, 20, 7] and now it is doubled it is [24, 6, 16, 40, 14]
            Initial Array- [45, 33, 42, 85, 35]
            Results - Your original array was [45, 33, 42, 85, 35] and now it is doubled it is [90, 66, 84, 170, 70]
            */

        }
        public static double SpaceCadet(double weight)
        {
            double moonWeight = weight / 6;             //divided the user prompted weight by 6 to get the moon weight
            return moonWeight;
        }
        public static decimal DiscountCalculator(decimal firstItem, decimal secondItem, decimal discountPercent)        //created new method to calculate the discount
        {
            decimal percent = discountPercent / 100;                     //divided percent by 100
            decimal discount = (firstItem + secondItem) * percent;              // added the cost of both items and multiplied it by the percent to get the amount of the discount
            decimal total = firstItem + secondItem - discount;          //minused the discount amount from the cost of both items
            return total;
            

        }

        public static int[] DoubleFun(int[] oldArray)
        {
            int[] newArray = new int[oldArray.Length];

            for (int i = 0; i < oldArray.Length; i++)
            {
                int itemDoubled = oldArray[i] + oldArray[i];
                newArray[i] = itemDoubled;
            }
            
            return newArray;
        }

    }
}
