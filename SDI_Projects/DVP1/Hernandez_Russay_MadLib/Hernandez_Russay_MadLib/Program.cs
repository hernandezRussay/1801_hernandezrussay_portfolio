﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hernandez_Russay_MadLib
{
    class Program
    {
        static void Main(string[] args)
        {          
           //Russay Hernandez
           //December 7, 2017
           //MadLib 
            
            
            /*list of arrays*/
            string[] myArray = new string[8];                   //created an array to hold my strings
            decimal[] myArray2 = new decimal[8];                //created an array to hold my integers
                    /*user prompts strings*/
            Console.WriteLine("give me one occupation?");        //promted the user 
            string newVer0 = Console.ReadLine();                   //placed the users response in a variable 

            Console.WriteLine("give me a color?");               //did the same for the following 3 prompts
            string newVer1 = Console.ReadLine();

            Console.WriteLine("give me a noun");
            string newVer2 = Console.ReadLine();

            Console.WriteLine("give me a verb");
            string newVer3 = Console.ReadLine();

            /*end of string*/

            Console.WriteLine("what is your age");                  //prompted the uer for a number
            string newVer4 = Console.ReadLine();                    //stored responce in a variable
            decimal numVer1 = decimal.Parse(newVer4);               //converted the responce to decimal
           

            Console.WriteLine("give me the cost of something");      //did the same for the following 2 prompts
            string newVer5 = Console.ReadLine();                     
            decimal numVer2 = decimal.Parse(newVer5);
            

            Console.WriteLine("give me a random number");
            string newVer6 = Console.ReadLine();
            decimal numVer3 = decimal.Parse(newVer6);




            /*string arrays*/
            myArray[0] = newVer0;               //stored the value of my variables in my array 
            myArray[1] = newVer1;           
            myArray[2] = newVer2;
            myArray[3] = newVer3;

            /*number arrays*/
            myArray2[4] = numVer1;              //stored the converted variable in a number array
            myArray2[5] = numVer2;
            myArray2[6] = numVer3;
           
            /*final display*/
            Console.WriteLine("Once upon a time, a "+myArray[0]+" was walking down a "+myArray[1]+      //created a story based on the input gattered in the array
                " boardwalk next to the beach, he wasnt alone though, with him was his "+myArray2[4]+
                " year old daughter. They were hungry so they saw a nice restaurant they could eat in. "+myArray2[5]+
                " was a little expensive for a meal but he wanted to show his daughter a good time. Next thing you know, they both got up and "+myArray[3]+
                " because "+myArray[2]+" walked in the restaurant. Before they could leave however, our dear "+myArray[0]+
                " got a bill for "+myArray2[5]+". The bill was too high so they both made a run for it.") ;
        }
    }
}
