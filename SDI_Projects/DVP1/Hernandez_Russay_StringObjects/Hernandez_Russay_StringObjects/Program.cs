﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Hernandez_Russay_StringObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Russay Hernandez
            December 27th, 2017
            String Objects
            */


            Console.WriteLine("Enter a phone number in the pattern (XXX) XXX-XXXX: ");      //prompted the user for a telephone number in the format
            string telNo = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(telNo))      //validated the input to make sure it wasn't left blank
            {
                Console.WriteLine("Please don't leave this blank. Enter a phone number in the pattern: ");
                telNo = Console.ReadLine();
            }

            string valid = IsPhoneNumber(telNo);    //calling the custom function IsPhoneNumber with the telNo string as the parameter and setting it to the variable valid

            if (valid == "valid")           //checking if it's valid or not and printing to the console
            {
                Console.WriteLine("The phone number you entered of {0} is a valid phone number", telNo);
            } else if (valid == "invalid")
            {
                Console.WriteLine("The phone number you entered of {0} is an invalid phone number", telNo);
            }

            /*
            Data Sets To Test:
            User Inputted Phone Number – (123) 456-7890
            Results - "The phone number you entered of (123) 456-7890 is a valid phone number."
            User Inputted Phone Number – (123 456-7890
            Results - "The phone number you entered of (123 456-7890 is an invalid phone number."
            User Inputted Phone Number – 123-456-7890
            Results - "The phone number you entered of 123-456-7890 is an invalid phone number."
            User Inputted Phone Number – 1234567890
            Results - "The phone number you entered of 1234567890 is an invalid phone number."
            User Inputted Phone Number – (800) 692-7753
            Results - "The phone number you entered of (800) 692-7753 is a valid phone number."
            */

            //

            Console.WriteLine("Enter encrypted text string: ");    //prompted the user to  enter an encrpted text
            string enC = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(enC))    //validated to make sure it wasn't left blank
            {
                Console.WriteLine("Please do no leave this blank. Enter encrypted string: ");
                enC = Console.ReadLine();
            }

            string decrypt = Decipher(enC);    //defined a new string variable to call the custom method Decipher() with enC as the parameter

            Console.WriteLine("The encrypted string is {0}. Deciphered the string is {1}.", enC, decrypt);   //outputted the result to the console


            /*
            Data Sets To Test:
            Encrypted String #1 – "Str^ng *bj#cts c@n b# f+n t* w*rk w^th!"
            Results - "String objects can be fun to work with!"
            Encrypted String #2 – "D#b+gg^ng ^s th# pr*c#ss *f r#m*v^ng s*ftw@r# b+gs, th#n pr*gr@mm^ng m+st b# th# pr*c#ss *f p+tt^ng th#m ^n."
            Results - "Debugging is the process of removing software bugs, then programming must be the process of putting them in."
            Encrypted String #3 – "M#rry Chr^stm@s #v#ry*n#!"
            Results - "Merry Christmas Everyone!"
            */


        }

        public static string IsPhoneNumber(string number)
        {
            string valid;    //declared a new string variable to hold the result and returning it to Main

            if (number.IndexOf("(") != 0)   //Checked if the right symbol is at the correct index number, and returned "invalid" if not
            {
                valid = "invalid";
                return valid;
            }
            else if (number.IndexOf(")") != 4)
            {
                valid = "invalid";
                return valid;
            }
            else if (number.IndexOf(" ") != 5)
            {
                valid = "invalid";
                return valid;
            }
            else if (number.IndexOf("-") != 9)
            {
                valid = "invalid";
                return valid;
            }
            else if (number.Length != 14)
            {
                valid = "invalid";
                return valid;
            }
            else    //if all the above tests passed it sends valid to the Main
            {
                valid = "valid";
                return valid;
            }
            

    
        }


        public static string Decipher(string enC)   //defined custom method
        {
            char[] enCArray = enC.ToCharArray();          //defined a character array and used .ToCharArray() to change the user inputted string to an array to check each character

            string newEnC = "";   //defined a new string to store the changed characters

            for (int i = 0; i < enC.Length; i++)   //used a for loop to cycle through the old string and check each character, see if it needs decrypting, and changing it if so
            {
                char letter = enCArray[i];
                if (letter == '@')
                {
                    letter = 'a';
                }
                else if (letter == '#')
                {
                    letter = 'e';
                }
                else if (letter == '^')
                {
                    letter = 'i';
                }
                else if (letter == '*')
                {
                    letter = 'o';
                }
                else if (letter == '+')
                {
                    letter = 'u';
                }
                newEnC += letter;    //adding the decrypted letter to the new string
            }
            return newEnC;   //returning the new string

            



        }


    }
}
