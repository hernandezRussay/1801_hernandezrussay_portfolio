﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hernandez_russay_restaurantCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            //Russay Hernandez
            //december 6, 2017
            //Tip Calculator
            Console.WriteLine("please give me the price of your first meal"); //here i promted the user for the costs of his meals individually
            string firstMeal = Console.ReadLine(); //created a variable to catch the users input
            Console.WriteLine("tip youd like to give");//promted the user for the tip hed like to give
            string firstTip = Console.ReadLine();//caught the users input
            decimal numVal1 = decimal.Parse(firstMeal);//coverted the strings to decimals to use operations
            decimal numVal2 = decimal.Parse(firstTip);//converted the users input to decimal in order to use operators
            decimal tip1 = numVal1 / 100m * numVal2; //used operators to find the percentage of the users meal
            decimal total1 = tip1 + numVal1; // i added the cost of the meal to the tip for a total cost

            Console.WriteLine("with your tip of $" + tip1 + " your total come out to $" + total1);// here i displayed to the user his tip amount and total cost
            //repeated the same process with the other 2 meals
            /*new meal cost*/
            Console.WriteLine("please give me the price of your second meal");
            string secondMeal = Console.ReadLine();
            Console.WriteLine("tip youd like to give");
            string secondTip = Console.ReadLine();
            decimal numVal3 = decimal.Parse(secondMeal);
            decimal numVal4 = decimal.Parse(secondTip);
            decimal tip2 = numVal3 / 100m * numVal4;
            decimal total2 = tip2 + numVal3;
            Console.WriteLine("with your tip of $" + tip2 + " your total come out to $" + total2);
            /*new meal cost*/
            Console.WriteLine("please give me the price of your third meal");
            string thirdMeal = Console.ReadLine();
            Console.WriteLine("tip youd like to give");
            string thirdTip = Console.ReadLine();
            decimal numVal5 = decimal.Parse(thirdMeal);
            decimal numVal6 = decimal.Parse(thirdTip);
            decimal tip3 = numVal5 / 100m * numVal6;
            decimal total3 = tip3 + numVal5;
            Console.WriteLine("with your tip of $" + tip3 + " your total come out to $" + total3);
            /*grand totals*/
            decimal totalTip = tip1 + tip2 + tip3; // added the total tips
            decimal totalCost = totalTip + total1 + total2 + total3;  //created a variable to hold total cost, added total tips to the cost of each meal


            Console.WriteLine("total tip for your waiter $" + totalTip);//displayed total tip for the waiter
            Console.WriteLine("your grand total come out to $" + totalCost);//displayed total including tip
            Console.WriteLine("your bill split evenly would be $" + totalCost / 3); //displayed the bill split 3 ways
        }
    }
}
