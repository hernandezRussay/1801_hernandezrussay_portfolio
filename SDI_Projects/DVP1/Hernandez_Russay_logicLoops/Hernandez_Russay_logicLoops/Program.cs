﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hernandez_Russay_logicLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            //Russay Hernandez
            //December 9, 2017
            //logic loops
                        /*Problem 1*/

            Console.WriteLine("Please enter your first grade");            //prompted the user for first grade
            string firstGrade = Console.ReadLine();                        //stored his response in a variable

            double user1;

            while (!double.TryParse(firstGrade, out user1))                //validated the user to ensure i get numbers and not letters
            {
                Console.WriteLine("Please re enter your first grade");     //prompted the user to re enter information should he type a letter instead of a number
                firstGrade = Console.ReadLine();
            }

            Console.WriteLine("Please enter your second grade");           //prompted the user for a second grade
            string secondGrade = Console.ReadLine();                       //stored his response in a variable
            
            
            while (!double.TryParse(secondGrade, out user1))               //validated the user to ensure i get numbers and not letters
            {
                Console.WriteLine("Please re enter your second grade");    //prompted the user to re enter information should he type a letter instead of a number
                secondGrade = Console.ReadLine();
            }


            double firstGrade1 = double.Parse(firstGrade);                 //converted the string response to double
            double secondGrade1 = double.Parse(secondGrade);

            double[] myArray = new double[7];                              //created an array to hold the grades

            myArray[0] = firstGrade1;                                      //stored the first and second grade in the array indexes
            myArray[1] = secondGrade1;

            if (myArray[0] < 70 && myArray[1]< 70){                        // created a if else statement to dispay that is the grades
                Console.WriteLine("One or more of your grades are failing, try harder next time");          // are less than 70 he has to try harder
            }else if (myArray[0] >= 70 && myArray[1] >= 70)
            {
                Console.WriteLine("Congrat! you passed the class");        //if both grades are over 70 then he passes
            }
            /*
            Grade 1 - 95 Grade 2 - 85 Results "Congrats! you passed the class"
            Grade 1 - 82 Grade 2 - 68 Results "One or more of your grades are failing, try harder next time"  
            Grade 1 - eighty - re prompt Grade 1 - 80 Grade 2 - 91 Results "Congrats! you passed the class"
            Grade 1 - 70 Grade 2 - 90 Results "Congrats! you passed the class"
            
            
            */
            /*problem 2*/

            Console.WriteLine("Please enter the age of the customer");
            string age = Console.ReadLine();

            while (!double.TryParse(age, out user1))                       //validated the user to ensure i get numbers and not letters
            {
                Console.WriteLine("Please re enter your customers age");   //prompted the user to re enter information should he type a letter instead of a number
                age = Console.ReadLine();
            }

            double age2 = double.Parse(age);                               //converted age from string to double

                   if (age2 < 10)  {                                       //created an if else if. if age is less than 10 then the price is 10, if equal or greater than ten is full price
                Console.WriteLine("Your cost for brunch is $10.00.");      //and if greater than 50 then they pay 10
            } else if (age2 >= 10) {
                Console.WriteLine("Your cost for brunch is $15.00.");
            } else if (age2 >= 50) {
                Console.WriteLine("Your cost for brunch is $10.00.");
            };

            /*
                        Age	– 57 Your cost for brunch today is $10.00
                        Age	– 39 Your cost for brunch today is $15.00
                        Age	– 8	 Your cost for brunch today is $10.00
                        Age	– 10 Your cost for brunch today is $15.00
                        Age - 101 Your cost for brunch today is $10.00
            */
            /*Problem 3*/
            Console.WriteLine("Amount of DVDs");
            string DVD = Console.ReadLine();

            while (!double.TryParse(DVD, out user1))                       //validated the user to ensure i get numbers and not letters
            {
                Console.WriteLine("Please re enter the amount of DVDs");   //prompted the user to re enter information should he type a letter instead of a number
                DVD = Console.ReadLine();
            }

            Console.WriteLine("Amount of BlueRays");
            string Blue = Console.ReadLine();

            while (!double.TryParse(Blue, out user1))                       //validated the user to ensure i get numbers and not letters
            {
                Console.WriteLine("Please re enter the amount of BlueRays");   //prompted the user to re enter information should he type a letter instead of a number
                Blue = Console.ReadLine();
            }

            Console.WriteLine("Amount of UltraViolets");
            string Ultra = Console.ReadLine();

            while (!double.TryParse(Ultra, out user1))                       //validated the user to ensure i get numbers and not letters
            {
                Console.WriteLine("Please re enter your UltraViolets");   //prompted the user to re enter information should he type a letter instead of a number
                Ultra = Console.ReadLine();
            }

            double DVD2 = double.Parse(DVD);                    //coverted all the users responses in double 
            double Blue2 = double.Parse(Blue);
            double Ultra2 = double.Parse(Ultra);

            double[] myArray2 = new double[3];              //created an array to hold the number of movies

            myArray2[0] = DVD2;                             //stored the users responses with three of the index
            myArray2[1] = Blue2;
            myArray2[2] = Ultra2;

            double totalSum = 0;

            foreach (double arrayItem in myArray2) {        //created a foreach loop to cycle though the array
                totalSum = totalSum + arrayItem;            //added total sum with the array items to get a total amount of movies
            };

            if (totalSum <= 100) {                          //if less than 100 displayed this line with the total sum
                Console.WriteLine("You have a total of "+totalSum+" movies in your collection");
            }
            else {                                          // else if over 100 displayed this line with the total sum
                Console.WriteLine("Wow, I am impressed with your collection of "+totalSum+" movies");
            }

            /*
            DVD - 45 Blue-Rays - 15 UltraViolet - 2
            You have a total of 62 movies in your collection
            DVD - 45 Blue-Rays - 15 UltraViolet - 2
            Wow, I am impressed with your collection of 180 movies!
            DVD - 22 Blue-Rays - 10 UltraViolet - 5
            You have a total of 37 movies in your collection
            */

            /*Problem 4*/

            Console.WriteLine("Initial Gift Card amount");
            string inCard = Console.ReadLine();                   // promted the user for an initial amount for their gift card
            double inCard2 = double.Parse(inCard);                // converted that string to double


            while (inCard2 > 0) {                               //created a while loop to loop through until the amount on the card hit 0

                Console.WriteLine("How much is the purchase"); // inside the loop, promted the user for the purchase amount
                string purPrice = Console.ReadLine();          //caught the users responce in a variable
                double purPrice2 = double.Parse(purPrice);     //converted
                inCard2 = inCard2 - purPrice2;                // subtracted the amount spent on each loop from the card amount
                Console.WriteLine("With your purchase of "+purPrice2+" your balance on the card is "+inCard2+" dollars.");
                if (inCard2 == 0) {                             //when the loop stops at exactly 0, displays this
                    Console.WriteLine("You spent all your money, your balance is " + inCard2 + " dollars.");
                } else if (inCard2 <= 0)                        //if they go over the card amount, displays this
                {
                    Console.WriteLine("You spent all your money and still owe "+inCard2*-1+" dollars.");
                }

            }
                        /*Gift card amount - 30
            purchase one - 5.00
            With your purchase of 5.00 your balance on the card is 25.00
            purchase two - 10.50
            With your purchase of 10.50 your balance on the card is 14.50
            purchase three - 16.00
            You spent all your money and still owe 1.50 dollars

                        /*Gift card amount - 80
            purchase one - 10.00
            With your purchase of 5.00 your balance on the card is 25.00
            purchase two - 20.00
            With your purchase of 10.50 your balance on the card is 14.50
            purchase three - 50.00
            You spent all your money, your balance is 0 dollars.
                         */







        }
    }
}
