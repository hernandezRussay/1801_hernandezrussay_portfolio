﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hernandez_Russay_Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             
             russay hernandez
            
             MDV2330-O
             
             December 6/ 2017

             Arrays Assignment
             */

            //Create your own project and call it Lastname_Firstname_Arrays
            //Copy this code inside of this Main section into your Main Section
            //Work through each of the sections

            /* 
              -Use the 2 arrays below
              -Do Not Re-Declare or Re-Define the elements inside of it.
              -Find the total sum of each individual array  
              -Store each sum in a variable and output that variable to console

              
              -Find the average value of each individual array
              -Store each average in a variable and output that variable to console
            */


            //This is the Declare and Definition of the 2 Starting Number Arrays
            int[] firstArray = new int[4] { 7, 24, 30, 12 };
            double[] secondArray = new double[4] { 9, 20.5, 35.9, 237.24 };

            int sum1 = firstArray.Sum();
            Console.WriteLine("total sum of array 1 is :" + sum1);

            double sum2 = secondArray.Sum();
            Console.WriteLine("total sum of array 2 is :" + sum2);

            int average1 = sum1 / firstArray.Length;
            Console.WriteLine("the average of array 1 is :" + average1);

            double average2 = sum2 / secondArray.Length;
            Console.WriteLine("the average of array 2 is :" + average2);




            /*
               Create a 3rd number array.  
               The values of this array will come from the 2 given arrays.
                -You will take the first item in each of the 2 number arrays, add them together and then store this sum inside of the new array.
                -For example Add the index#0 of array 1 to index#0 of array2 and store this inside of your new array at the index#0 spot.
                    -This would make the first element of this array = 16
                -Repeat this for each index #.
                -Do not add them by hand, the computer must add them.
                -Do not use the numbers themselves, use the array elements.
                -After you have the completed new array, output this to the Console.
             */

            //Your code for the 3d Array goes here
            int[] myArray = new int[4];




            int newArray1 = (int)secondArray[0] + firstArray[0];
            int newArray2 = (int)secondArray[1] + firstArray[1];
            int newArray3 = (int)secondArray[2] + firstArray[2];
            int newArray4 = (int)secondArray[3] + firstArray[3];

            myArray[0] = newArray1;
            myArray[1] = newArray2;
            myArray[2] = newArray3;
            myArray[3] = newArray4;


            Console.WriteLine("the sum of the first index of the two arrays are :" + myArray[0]);
            Console.WriteLine("the sum of the second index of the two arrays are :" + myArray[1]);
            Console.WriteLine("the sum of the third index of the two arrays are :" + myArray[2]);
            Console.WriteLine("the sum of the forth index of the two arrays are :" + myArray[3]);



            /*
               Given the array of strings below named MixedUp.  
               You must create a string variable that puts the items in the correct order to make a complete sentence 
               that is a famous phrase by Bill Gates.
                -Use each element in the array, do not re-write the strings themselves.
                - Do Not Re-Declare or Re-Define the elements inside of it.
                -Concatenate them in the correct order to form a sentence.
                -Store this new sentence string inside of a string variable you create.
                -Output this new string variable to the console.
             */

            //Declare and Define The String Array
            string[] MixedUp = new string[] { " is like measuring ", "Measuring programming progress", "aircraft building progress ", " by lines of code", "by weight." };

            string quote0 = MixedUp[0];
            string quote1 = MixedUp[1];
            string quote2 = MixedUp[2];
            string quote3 = MixedUp[3];
            Console.WriteLine(quote1 + quote3 + quote0 + quote2);

        }
    }
}
