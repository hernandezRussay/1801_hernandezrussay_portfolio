﻿namespace EmployeeTrackingSoftware
{
    class Salaried : Employee
    {
        protected decimal salary;

        public Salaried(string name, string address, decimal salary) : base(name, address)
        {
            this.salary = salary;
        }

        public override decimal CalculatePay()
        {
            return salary;
        }
    }
}
