﻿using System;

/// <summary>
/// Russay Hernandez
/// 4/20/2018
/// MDV2430-O
/// </summary>

namespace EmployeeTrackingSoftware
{
    class Employee : IComparable
    {
        protected string name;
        protected string address;

        public Employee(string name, string address)
        {
            this.name = name;
            this.address = address;
        }

        public virtual decimal CalculatePay()
        {
            return 0;
        }

        public int CompareTo(object obj)
        {
            Employee emp = obj as Employee;
            return name.CompareTo(emp.name);
        }

        public override string ToString()
        {
            return string.Format("Name: {0}, Address: {1}, YearlyPay: {2:C}", name, address, CalculatePay());
        }
    }
}
