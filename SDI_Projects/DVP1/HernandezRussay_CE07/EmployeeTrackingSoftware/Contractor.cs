﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Russay Hernandez
/// 4/20/2018
/// MDV2430-O
/// </summary>

namespace EmployeeTrackingSoftware
{
    class Contractor : Hourly
    {
        protected decimal noBenefitsBonus;

        public Contractor(string name, string address, decimal payPerHour, decimal hoursPerWeek, decimal noBenefitsBonus) 
            : base(name, address, payPerHour, hoursPerWeek)
        {
            this.noBenefitsBonus = noBenefitsBonus;
        }

        public override decimal CalculatePay()
        {
            decimal baseAmount = payPerHour * hoursPerWeek;
            return baseAmount + baseAmount * noBenefitsBonus;
        }
    }
}
