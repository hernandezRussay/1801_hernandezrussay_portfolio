﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Russay Hernandez
/// 4/20/2018
/// MDV2430-O
/// </summary>

namespace EmployeeTrackingSoftware
{
    class Program
    {
        const string TITLE = "Employee Tracking Software";
        static readonly Dictionary<string, string> commands = new Dictionary<string, string>()
        {
            { "Add Employee", "add" },
            { "Remove Employee", "remove" },
            { "Display Payroll", "display" },
            { "Exit", "exit" }
        };

        static void Main(string[] args)
        {
            Console.Title = TITLE;

            List<Employee> employee = new List<Employee>();

            while (true)
            {
                ShowMenu();
                string cmd = SelectMenuItem();

                if(string.IsNullOrEmpty(cmd))
                {
                    Console.Clear();
                    Console.WriteLine("Command not found, please, try again.\n");
                }
                else
                {
                    //we have correct command
                    Console.Clear();
                    ExecuteCommand(cmd, employee);
                }
            }
        }

        static void ShowMenu()
        {
            Console.Write("*** Menu ***");

            int index = 1;
            foreach (var command in commands)
            {
                Console.Write("\n{0}.{1}", index++, command.Key);
            }
            Console.WriteLine();
        }

        /// <summary>
        /// returns command
        /// </summary>
        static string SelectMenuItem()
        {
            Console.Write("\nSelect command: ");
            string cmd = Console.ReadLine();

            if (string.IsNullOrEmpty(cmd))
                return null;
            else
            {
                cmd = cmd.ToLower();

                switch (cmd)
                {
                    case "1":
                    case "add employee":
                        return "add";
                    case "2":
                    case "remove employee":
                        return "remove";
                    case "3":
                    case "display payroll":
                        return "display";
                    case "4":
                    case "exit":
                        return "exit";
                }
            }

            return null;
        }

        static void ExecuteCommand(string command, List<Employee> employee)
        {
            switch (command)
            {
                case "add":
                    AddEmployee(employee);
                    employee.Sort();
                    break;
                case "remove":
                    RemoveEmployee(employee);
                    break;
                case "display":
                    DisplayEmployee(employee);
                    break;
                default:
                    Environment.Exit(0);
                    break;
            }

            Console.Write("\nPress any key to go to the menu.");
            Console.ReadKey();
            Console.Clear();
        }

        static void DisplayEmployee(List<Employee> employee)
        {
            Console.WriteLine("List of employees:");
            int index = 1;
            foreach (var emp in employee)
                Console.WriteLine("{0}.{1}", index++, emp.ToString());
        }
        static void RemoveEmployee(List<Employee> employee)
        {
            if(employee.Count == 0)
            {
                Console.WriteLine("There are no employees in the list.");
                return;
            }

            int selectedEmployee = 0;
            while (selectedEmployee <= 0)
            {
                if (selectedEmployee < 0)
                    Console.WriteLine("Wrong number of employee, try again.\n");

                DisplayEmployee(employee);
                selectedEmployee = SelectEmployee(employee.Count);
                Console.Clear();
            }

            employee.RemoveAt(selectedEmployee - 1);
            Console.WriteLine("Employee was successfully removed.");
        }
        static void AddEmployee(List<Employee> employee)
        {
            EmployeeType type = SelectEmployeeType();

            Console.Clear();
            Console.WriteLine("\tAdd Employee - {0}", type.ToString());

            Console.Write("Input name: ");
            string name =  Console.ReadLine();

            Console.Write("Input address: ");
            string address = Console.ReadLine();

            AddEmployeeByType(type, name, address, employee);

            Console.WriteLine("Employee was successfully added.");
        }

        static int SelectEmployee(int countOfEmployees)
        {
            Console.Write("\nSelect number of employee: ");
            string number = Console.ReadLine();

            int selectedEmployee;
            if(int.TryParse(number, out selectedEmployee) && selectedEmployee > 0 && selectedEmployee <= countOfEmployees)
                return selectedEmployee;

            return -1;
        }
        static EmployeeType SelectEmployeeType()
        {
            EmployeeType? empType = null;
            

            while (!empType.HasValue)
            {
                int countOfEmpTypes = ShowEmployeesType();

                Console.Write("\nChoose employee type: ");
                int index = 0;
                if(int.TryParse(Console.ReadLine(), out index) && index > 0 && index <= countOfEmpTypes)
                {
                    empType = (EmployeeType)index;
                }
                else
                {
                    Console.Clear();
                    Console.Write("Incorrect number of type.\n\n");
                }
            }

            return empType.Value;
        }
        static int ShowEmployeesType()
        {
            Console.WriteLine("List of employee types:");
            string[] employeesType = Enum.GetNames(typeof(EmployeeType));
            for (int i = 0; i < employeesType.Length; i++)
                Console.WriteLine("{0}.{1}", i + 1, employeesType[i]);

            return employeesType.Length;
        }
        static void AddEmployeeByType(EmployeeType type, string name, string address, List<Employee> employee)
        {
            decimal payPerHour,
                hoursPerWeek,
                noBenefitsBonus,
                salary,
                bonus;

            switch (type)
            {
                case EmployeeType.FullTime:
                    payPerHour = InputDecimal("Input pay per hour: ");
                    employee.Add(new FullTime(name, address, payPerHour));
                    break;
                case EmployeeType.PartTime:
                    payPerHour = InputDecimal("Input pay per hour: ");
                    hoursPerWeek = InputDecimal("Input hours per week: ");
                    employee.Add(new PartTime(name, address, payPerHour, hoursPerWeek));
                    break;
                case EmployeeType.Contractor:
                    payPerHour = InputDecimal("Input pay per hour: ");
                    hoursPerWeek = InputDecimal("Input hours per week: ");
                    noBenefitsBonus = InputBenefitsBonus();
                    employee.Add(new Contractor(name, address, payPerHour, hoursPerWeek, noBenefitsBonus));
                    break;
                case EmployeeType.Salaried:
                    salary = InputDecimal("Input salary: ");
                    employee.Add(new Salaried(name, address, salary));
                    break;
                case EmployeeType.Manager:
                    salary = InputDecimal("Input salary: ");
                    bonus = InputDecimal("Input bonus: ");
                    employee.Add(new Manager(name, address, salary, bonus));
                    break;
            }
        }
           
        static decimal InputDecimal(string message)
        {
            decimal value = -1;

            while (value < 0)
            {
                Console.Write(message);
                if(!(decimal.TryParse(Console.ReadLine(), out value) && value >= 0))
                {
                    value = -1;
                    Console.WriteLine("Incorrect value. Please, try again.");
                }
            }

            return value;
        }
        static decimal InputBenefitsBonus()
        {
            decimal value = -1;

            while (value < 0)
            {
                Console.Write("Input benefits bonus in percents: ");
                if (!(decimal.TryParse(Console.ReadLine(), out value) && value >= 0))
                {
                    value = -1;
                    Console.WriteLine("Incorrect value. Please, try again.");
                }
            }

            return value / 100;
        }
    }


    public enum EmployeeType
    {
        FullTime = 1,
        PartTime,
        Contractor,
        Salaried,
        Manager
    };
}
