﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Russay Hernandez
/// 4/20/2018
/// MDV2430-O
/// </summary>

namespace EmployeeTrackingSoftware
{
    class Hourly : Employee
    {
        protected decimal payPerHour;
        protected decimal hoursPerWeek;

        public Hourly(string name, string address, decimal payPerHour, decimal hoursPerWeek) : base(name, address)
        {
            this.payPerHour = payPerHour;
            this.hoursPerWeek = hoursPerWeek;
        }

        public override decimal CalculatePay()
        {
            return payPerHour * hoursPerWeek;
        }
    }
}
