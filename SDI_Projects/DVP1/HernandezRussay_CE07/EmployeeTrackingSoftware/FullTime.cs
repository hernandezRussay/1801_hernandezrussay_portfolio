﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Russay Hernandez
/// 4/20/2018
/// MDV2430-O
/// </summary>

namespace EmployeeTrackingSoftware
{
    class FullTime : Hourly
    {
        private const decimal HOURS_PER_WEEK = 40;
        public FullTime(string name, string address, decimal payPerHour) : base(name, address, payPerHour, HOURS_PER_WEEK)
        {

        }
    }
}
