﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Russay Hernandez
/// 4/20/2018
/// MDV2430-O
/// </summary>

namespace EmployeeTrackingSoftware
{
    class Manager: Salaried
    {
        protected decimal bonus;

        public Manager(string name, string address, decimal salary, decimal bonus) : base(name, address, salary)
        {
            this.bonus = bonus;
        }

        public override decimal CalculatePay()
        {
            return salary + bonus;
        }
    }
}
