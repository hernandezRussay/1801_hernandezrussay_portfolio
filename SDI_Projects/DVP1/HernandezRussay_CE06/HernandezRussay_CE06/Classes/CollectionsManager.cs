﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Russay Hernandez
/// 4/20/2018
/// MDV2430-O
/// </summary>

namespace CollectionManagerApp
{
    public class CollectionsManager
    {
        /// <summary>
        ///  List for the list of available cards 
        /// </summary>
        public List<Card> availableCards;

        /// <summary>
        /// a Dictionary > to hold multiple collections 
        /// each collection(List of cards) is accessed with a string
        /// </summary>
        public Dictionary<string, List<Card>> allCollections;


        /// <summary>
        /// Ctor
        /// </summary>
        public CollectionsManager()
        {
            this.availableCards = new List<Card>();
            this.allCollections = new Dictionary<string, List<Card>>();
        }
    }
}
