﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Russay Hernandez
/// 4/20/2018
/// MDV2430-O
/// </summary>

namespace CollectionManagerApp
{
    public class Card
    {
        /// <summary>
        /// Card's name property with private set
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Card's description property with private set
        /// </summary>
        public string Description { get; private set; }
        /// <summary>
        /// Card's value property with private set
        /// </summary>
        public decimal Value { get; private set; }

        /// <summary>
        /// ctor with parameters
        /// </summary>
        public Card(string name, string description, decimal value)
        {
            this.Name = name;
            this.Description = description;
            this.Value = value;
        }
    }
}
