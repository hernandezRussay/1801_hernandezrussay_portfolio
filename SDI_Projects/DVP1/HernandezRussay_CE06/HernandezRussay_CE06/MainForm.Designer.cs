﻿namespace CollectionManagerApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.createCollectionsManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collectionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createCollectionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.displaySelectedCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayAllCollectionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cardsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createACardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addSelectedCardToSelectedCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeSelectedCardFromSelectedCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveCardBetweenCollectionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.availableCardDescriptionTbx = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.availableCardValueTbx = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.availableCardslistBox = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.collectionNameTbx = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.descriptionTbx = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.valueTbx = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.cardsInCollectionlistBox = new System.Windows.Forms.ListBox();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createCollectionsManagerToolStripMenuItem,
            this.collectionsToolStripMenuItem,
            this.cardsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(673, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // createCollectionsManagerToolStripMenuItem
            // 
            this.createCollectionsManagerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createToolStripMenuItem});
            this.createCollectionsManagerToolStripMenuItem.Name = "createCollectionsManagerToolStripMenuItem";
            this.createCollectionsManagerToolStripMenuItem.Size = new System.Drawing.Size(128, 20);
            this.createCollectionsManagerToolStripMenuItem.Text = "Collections manager";
            // 
            // createToolStripMenuItem
            // 
            this.createToolStripMenuItem.Name = "createToolStripMenuItem";
            this.createToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.createToolStripMenuItem.Text = "Create";
            this.createToolStripMenuItem.Click += new System.EventHandler(this.createToolStripMenuItem_Click);
            // 
            // collectionsToolStripMenuItem
            // 
            this.collectionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createCollectionToolStripMenuItem1,
            this.displaySelectedCollectionToolStripMenuItem,
            this.displayAllCollectionsToolStripMenuItem});
            this.collectionsToolStripMenuItem.Enabled = false;
            this.collectionsToolStripMenuItem.Name = "collectionsToolStripMenuItem";
            this.collectionsToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.collectionsToolStripMenuItem.Text = "Collections";
            // 
            // createCollectionToolStripMenuItem1
            // 
            this.createCollectionToolStripMenuItem1.Name = "createCollectionToolStripMenuItem1";
            this.createCollectionToolStripMenuItem1.Size = new System.Drawing.Size(213, 22);
            this.createCollectionToolStripMenuItem1.Text = "Create collection";
            this.createCollectionToolStripMenuItem1.Click += new System.EventHandler(this.createCollectionToolStripMenuItem1_Click);
            // 
            // displaySelectedCollectionToolStripMenuItem
            // 
            this.displaySelectedCollectionToolStripMenuItem.Name = "displaySelectedCollectionToolStripMenuItem";
            this.displaySelectedCollectionToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.displaySelectedCollectionToolStripMenuItem.Text = "Display selected collection";
            this.displaySelectedCollectionToolStripMenuItem.Click += new System.EventHandler(this.displaySelectedCollectionToolStripMenuItem_Click);
            // 
            // displayAllCollectionsToolStripMenuItem
            // 
            this.displayAllCollectionsToolStripMenuItem.Name = "displayAllCollectionsToolStripMenuItem";
            this.displayAllCollectionsToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.displayAllCollectionsToolStripMenuItem.Text = "Display all collections";
            this.displayAllCollectionsToolStripMenuItem.Click += new System.EventHandler(this.displayAllCollectionsToolStripMenuItem_Click);
            // 
            // cardsToolStripMenuItem
            // 
            this.cardsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createACardToolStripMenuItem,
            this.addSelectedCardToSelectedCollectionToolStripMenuItem,
            this.removeSelectedCardFromSelectedCollectionToolStripMenuItem,
            this.moveCardBetweenCollectionsToolStripMenuItem});
            this.cardsToolStripMenuItem.Enabled = false;
            this.cardsToolStripMenuItem.Name = "cardsToolStripMenuItem";
            this.cardsToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.cardsToolStripMenuItem.Text = "Cards";
            // 
            // createACardToolStripMenuItem
            // 
            this.createACardToolStripMenuItem.Name = "createACardToolStripMenuItem";
            this.createACardToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.createACardToolStripMenuItem.Text = "Create a card";
            this.createACardToolStripMenuItem.Click += new System.EventHandler(this.createACardToolStripMenuItem_Click);
            // 
            // addSelectedCardToSelectedCollectionToolStripMenuItem
            // 
            this.addSelectedCardToSelectedCollectionToolStripMenuItem.Name = "addSelectedCardToSelectedCollectionToolStripMenuItem";
            this.addSelectedCardToSelectedCollectionToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.addSelectedCardToSelectedCollectionToolStripMenuItem.Text = "Add card to collection";
            this.addSelectedCardToSelectedCollectionToolStripMenuItem.Click += new System.EventHandler(this.addSelectedCardToSelectedCollectionToolStripMenuItem_Click);
            // 
            // removeSelectedCardFromSelectedCollectionToolStripMenuItem
            // 
            this.removeSelectedCardFromSelectedCollectionToolStripMenuItem.Name = "removeSelectedCardFromSelectedCollectionToolStripMenuItem";
            this.removeSelectedCardFromSelectedCollectionToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.removeSelectedCardFromSelectedCollectionToolStripMenuItem.Text = "Remove card from collection";
            this.removeSelectedCardFromSelectedCollectionToolStripMenuItem.Click += new System.EventHandler(this.removeSelectedCardFromSelectedCollectionToolStripMenuItem_Click);
            // 
            // moveCardBetweenCollectionsToolStripMenuItem
            // 
            this.moveCardBetweenCollectionsToolStripMenuItem.Name = "moveCardBetweenCollectionsToolStripMenuItem";
            this.moveCardBetweenCollectionsToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.moveCardBetweenCollectionsToolStripMenuItem.Text = "Move card between collections";
            this.moveCardBetweenCollectionsToolStripMenuItem.Click += new System.EventHandler(this.moveCardBetweenCollectionsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox1.Location = new System.Drawing.Point(325, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(348, 305);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Available Cards";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.availableCardDescriptionTbx);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(169, 17);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(176, 237);
            this.panel2.TabIndex = 5;
            // 
            // availableCardDescriptionTbx
            // 
            this.availableCardDescriptionTbx.BackColor = System.Drawing.Color.White;
            this.availableCardDescriptionTbx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableCardDescriptionTbx.Location = new System.Drawing.Point(0, 15);
            this.availableCardDescriptionTbx.Multiline = true;
            this.availableCardDescriptionTbx.Name = "availableCardDescriptionTbx";
            this.availableCardDescriptionTbx.ReadOnly = true;
            this.availableCardDescriptionTbx.Size = new System.Drawing.Size(176, 222);
            this.availableCardDescriptionTbx.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Description:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.availableCardValueTbx);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(163, 254);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(182, 48);
            this.panel1.TabIndex = 4;
            // 
            // availableCardValueTbx
            // 
            this.availableCardValueTbx.BackColor = System.Drawing.Color.White;
            this.availableCardValueTbx.Location = new System.Drawing.Point(50, 14);
            this.availableCardValueTbx.Name = "availableCardValueTbx";
            this.availableCardValueTbx.ReadOnly = true;
            this.availableCardValueTbx.Size = new System.Drawing.Size(129, 21);
            this.availableCardValueTbx.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Value:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.availableCardslistBox);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(3, 17);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(160, 285);
            this.panel3.TabIndex = 6;
            // 
            // availableCardslistBox
            // 
            this.availableCardslistBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableCardslistBox.FormattingEnabled = true;
            this.availableCardslistBox.ItemHeight = 15;
            this.availableCardslistBox.Location = new System.Drawing.Point(0, 0);
            this.availableCardslistBox.Name = "availableCardslistBox";
            this.availableCardslistBox.Size = new System.Drawing.Size(160, 285);
            this.availableCardslistBox.TabIndex = 1;
            this.availableCardslistBox.SelectedIndexChanged += new System.EventHandler(this.availableCardslistBox_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.collectionNameTbx);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(0, 24);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(320, 305);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Collection";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 15);
            this.label5.TabIndex = 6;
            this.label5.Text = "Collection name:";
            // 
            // collectionNameTbx
            // 
            this.collectionNameTbx.BackColor = System.Drawing.Color.White;
            this.collectionNameTbx.Location = new System.Drawing.Point(117, 20);
            this.collectionNameTbx.Name = "collectionNameTbx";
            this.collectionNameTbx.ReadOnly = true;
            this.collectionNameTbx.Size = new System.Drawing.Size(194, 21);
            this.collectionNameTbx.TabIndex = 5;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.panel4);
            this.groupBox4.Controls.Add(this.panel5);
            this.groupBox4.Controls.Add(this.panel6);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox4.Location = new System.Drawing.Point(3, 46);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(314, 256);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Collection cards";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.descriptionTbx);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(135, 17);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(176, 188);
            this.panel4.TabIndex = 8;
            // 
            // descriptionTbx
            // 
            this.descriptionTbx.BackColor = System.Drawing.Color.White;
            this.descriptionTbx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.descriptionTbx.Location = new System.Drawing.Point(0, 15);
            this.descriptionTbx.Multiline = true;
            this.descriptionTbx.Name = "descriptionTbx";
            this.descriptionTbx.ReadOnly = true;
            this.descriptionTbx.Size = new System.Drawing.Size(176, 173);
            this.descriptionTbx.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Description:";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.valueTbx);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(126, 205);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(185, 48);
            this.panel5.TabIndex = 7;
            // 
            // valueTbx
            // 
            this.valueTbx.BackColor = System.Drawing.Color.White;
            this.valueTbx.Location = new System.Drawing.Point(52, 17);
            this.valueTbx.Name = "valueTbx";
            this.valueTbx.ReadOnly = true;
            this.valueTbx.Size = new System.Drawing.Size(129, 21);
            this.valueTbx.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "Value:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.cardsInCollectionlistBox);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(3, 17);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(123, 236);
            this.panel6.TabIndex = 9;
            // 
            // cardsInCollectionlistBox
            // 
            this.cardsInCollectionlistBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cardsInCollectionlistBox.FormattingEnabled = true;
            this.cardsInCollectionlistBox.ItemHeight = 15;
            this.cardsInCollectionlistBox.Location = new System.Drawing.Point(0, 0);
            this.cardsInCollectionlistBox.Name = "cardsInCollectionlistBox";
            this.cardsInCollectionlistBox.Size = new System.Drawing.Size(123, 236);
            this.cardsInCollectionlistBox.TabIndex = 1;
            this.cardsInCollectionlistBox.SelectedIndexChanged += new System.EventHandler(this.cardsInCollectionlistBox_SelectedIndexChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 329);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Collections Manager";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem collectionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createCollectionToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem displaySelectedCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayAllCollectionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cardsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createACardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addSelectedCardToSelectedCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeSelectedCardFromSelectedCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox availableCardDescriptionTbx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox availableCardValueTbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListBox availableCardslistBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox descriptionTbx;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox valueTbx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ListBox cardsInCollectionlistBox;
        private System.Windows.Forms.ToolStripMenuItem createCollectionsManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem;
        private System.Windows.Forms.TextBox collectionNameTbx;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripMenuItem moveCardBetweenCollectionsToolStripMenuItem;
    }
}

