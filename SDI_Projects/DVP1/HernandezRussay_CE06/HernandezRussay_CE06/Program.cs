﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

/// <summary>
/// Russay Hernandez
/// 4/20/2018
/// MDV2430-O
/// </summary>

namespace CollectionManagerApp
{
    static class Program
    {
        /// <summary>
        /// Search in cardsList card by cardName and return Card object from cardsList 
        /// if card not found, then method returns null
        /// </summary>
        public static Card SearchCardInListByName(string cardName, List<Card> cardsList)
        {
            for (int i = 0; i < cardsList.Count; ++i)
            {
                if (cardsList[i].Name == cardName)
                {
                    return cardsList[i];
                }
            }

            return null;
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
