﻿namespace CollectionManagerApp
{
    partial class AddCardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddCardBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cardNameTbx = new System.Windows.Forms.TextBox();
            this.cardDescriptionTbx = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.value_nuD = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.value_nuD)).BeginInit();
            this.SuspendLayout();
            // 
            // AddCardBtn
            // 
            this.AddCardBtn.Location = new System.Drawing.Point(19, 182);
            this.AddCardBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AddCardBtn.Name = "AddCardBtn";
            this.AddCardBtn.Size = new System.Drawing.Size(342, 38);
            this.AddCardBtn.TabIndex = 0;
            this.AddCardBtn.Text = "Submit !";
            this.AddCardBtn.UseVisualStyleBackColor = true;
            this.AddCardBtn.Click += new System.EventHandler(this.AddCardBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Card\'s Name: ";
            // 
            // cardNameTbx
            // 
            this.cardNameTbx.Location = new System.Drawing.Point(121, 7);
            this.cardNameTbx.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cardNameTbx.MaxLength = 100;
            this.cardNameTbx.Name = "cardNameTbx";
            this.cardNameTbx.Size = new System.Drawing.Size(240, 22);
            this.cardNameTbx.TabIndex = 2;
            // 
            // cardDescriptionTbx
            // 
            this.cardDescriptionTbx.Location = new System.Drawing.Point(19, 84);
            this.cardDescriptionTbx.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cardDescriptionTbx.MaxLength = 1000;
            this.cardDescriptionTbx.Multiline = true;
            this.cardDescriptionTbx.Name = "cardDescriptionTbx";
            this.cardDescriptionTbx.Size = new System.Drawing.Size(342, 88);
            this.cardDescriptionTbx.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 64);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Card\'s Description: ";
            // 
            // value_nuD
            // 
            this.value_nuD.Location = new System.Drawing.Point(121, 37);
            this.value_nuD.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.value_nuD.Name = "value_nuD";
            this.value_nuD.Size = new System.Drawing.Size(70, 22);
            this.value_nuD.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 39);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Card\'s Value: ";
            // 
            // AddCardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 232);
            this.Controls.Add(this.value_nuD);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cardDescriptionTbx);
            this.Controls.Add(this.cardNameTbx);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AddCardBtn);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddCardForm";
            this.Text = "Add card";
            ((System.ComponentModel.ISupportInitialize)(this.value_nuD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddCardBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox cardNameTbx;
        private System.Windows.Forms.TextBox cardDescriptionTbx;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown value_nuD;
        private System.Windows.Forms.Label label2;
    }
}