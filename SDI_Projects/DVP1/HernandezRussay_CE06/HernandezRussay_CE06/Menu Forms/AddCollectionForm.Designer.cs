﻿namespace CollectionManagerApp
{
    partial class AddCollectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.collectionNameTbx = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AddCollectionBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // collectionNameTbx
            // 
            this.collectionNameTbx.Location = new System.Drawing.Point(134, 18);
            this.collectionNameTbx.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.collectionNameTbx.MaxLength = 100;
            this.collectionNameTbx.Name = "collectionNameTbx";
            this.collectionNameTbx.Size = new System.Drawing.Size(279, 21);
            this.collectionNameTbx.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Collection\'s Name: ";
            // 
            // AddCollectionBtn
            // 
            this.AddCollectionBtn.Location = new System.Drawing.Point(14, 56);
            this.AddCollectionBtn.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.AddCollectionBtn.Name = "AddCollectionBtn";
            this.AddCollectionBtn.Size = new System.Drawing.Size(399, 44);
            this.AddCollectionBtn.TabIndex = 3;
            this.AddCollectionBtn.Text = "Submit !";
            this.AddCollectionBtn.UseVisualStyleBackColor = true;
            this.AddCollectionBtn.Click += new System.EventHandler(this.AddCollectionBtn_Click);
            // 
            // AddCollectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 117);
            this.Controls.Add(this.collectionNameTbx);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AddCollectionBtn);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddCollectionForm";
            this.Text = "Add collection";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox collectionNameTbx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AddCollectionBtn;
    }
}