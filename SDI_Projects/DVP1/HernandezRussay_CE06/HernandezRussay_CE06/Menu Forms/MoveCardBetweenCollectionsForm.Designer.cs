﻿namespace CollectionManagerApp
{
    partial class MoveCardBetweenCollectionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.SubmitBtn = new System.Windows.Forms.Button();
            this.collectionCbox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cardsCbox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.secondCollectionCbx = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 24);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "Collection\'s Name: ";
            // 
            // SubmitBtn
            // 
            this.SubmitBtn.Location = new System.Drawing.Point(16, 164);
            this.SubmitBtn.Margin = new System.Windows.Forms.Padding(6);
            this.SubmitBtn.Name = "SubmitBtn";
            this.SubmitBtn.Size = new System.Drawing.Size(465, 51);
            this.SubmitBtn.TabIndex = 6;
            this.SubmitBtn.Text = "Submit !";
            this.SubmitBtn.UseVisualStyleBackColor = true;
            this.SubmitBtn.Click += new System.EventHandler(this.SubmitBtn_Click);
            // 
            // collectionCbox
            // 
            this.collectionCbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.collectionCbox.FormattingEnabled = true;
            this.collectionCbox.Location = new System.Drawing.Point(133, 20);
            this.collectionCbox.Name = "collectionCbox";
            this.collectionCbox.Size = new System.Drawing.Size(346, 23);
            this.collectionCbox.TabIndex = 8;
            this.collectionCbox.SelectedIndexChanged += new System.EventHandler(this.collectionCbox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "Card\'s Name: ";
            // 
            // cardsCbox
            // 
            this.cardsCbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cardsCbox.FormattingEnabled = true;
            this.cardsCbox.Location = new System.Drawing.Point(133, 49);
            this.cardsCbox.Name = "cardsCbox";
            this.cardsCbox.Size = new System.Drawing.Size(346, 23);
            this.cardsCbox.TabIndex = 8;
            this.cardsCbox.SelectedIndexChanged += new System.EventHandler(this.cardsCbox_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cardsCbox);
            this.groupBox1.Controls.Add(this.collectionCbox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(7, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(495, 87);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Choose card from collection";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.secondCollectionCbx);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(7, 105);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(495, 50);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Move to collection";
            // 
            // secondCollectionCbx
            // 
            this.secondCollectionCbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.secondCollectionCbx.FormattingEnabled = true;
            this.secondCollectionCbx.Location = new System.Drawing.Point(133, 20);
            this.secondCollectionCbx.Name = "secondCollectionCbx";
            this.secondCollectionCbx.Size = new System.Drawing.Size(346, 23);
            this.secondCollectionCbx.TabIndex = 8;
            this.secondCollectionCbx.SelectedIndexChanged += new System.EventHandler(this.secondCollectionCbx_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 24);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Collection\'s Name: ";
            // 
            // MoveCardBetweenCollectionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 230);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.SubmitBtn);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MoveCardBetweenCollectionsForm";
            this.Text = "Moving card between collections";
            this.Load += new System.EventHandler(this.SelectCollectionForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SubmitBtn;
        private System.Windows.Forms.ComboBox collectionCbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cardsCbox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox secondCollectionCbx;
        private System.Windows.Forms.Label label4;
    }
}