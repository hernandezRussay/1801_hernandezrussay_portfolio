﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CollectionManagerApp
{
    public partial class RemoveSelectedCardCollectionForm : Form
    {
        /// <summary>
        /// Selected collection Name
        /// </summary>
        public string SelectedCollection { get; private set; }

        /// <summary>
        /// Created card obj
        /// </summary>
        public Card Card { get; private set; }

        /// <summary>
        /// collections manager reference
        /// </summary>
        private CollectionsManager collectionsManager;

        public RemoveSelectedCardCollectionForm(CollectionsManager collectionsManager)
        {
            InitializeComponent();
            this.collectionsManager = collectionsManager;
            this.DialogResult = DialogResult.Cancel;
        }

        private void SelectCollectionForm_Load(object sender, EventArgs e)
        {
            // load all collection names
            collectionCbox.Items.Clear();
            for (int i = 0; i < this.collectionsManager.allCollections.Count; ++i)
            {
                collectionCbox.Items.Add(this.collectionsManager.allCollections.Keys.ElementAt(i));
            }
        }

        private void collectionCbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (collectionCbox.SelectedIndex > -1)
            {
                // load all cards in selected collection
                cardsCbox.Items.Clear();

                this.SelectedCollection = collectionCbox.Items[collectionCbox.SelectedIndex].ToString();
                List<Card> cardsInCollection = this.collectionsManager.allCollections[this.SelectedCollection];

                for (int i = 0; i < cardsInCollection.Count; ++i)
                {
                    cardsCbox.Items.Add(cardsInCollection[i].Name);
                }
            }
        }

        private void cardsCbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cardsCbox.SelectedIndex > -1)
            {
                // get selected collection
                List<Card> cardsInCollection = this.collectionsManager.allCollections[this.SelectedCollection];
                // get card name
                string cardName = cardsCbox.Items[cardsCbox.SelectedIndex].ToString();
                // search card in selected collection
                this.Card = Program.SearchCardInListByName(cardName, cardsInCollection);
            }
        }

        private void SubmitBtn_Click(object sender, EventArgs e)
        {
            // check input data
            if (string.IsNullOrEmpty(this.SelectedCollection))
            {
                MessageBox.Show("Choose collection name from list!");
                return;
            }

            if (this.Card == null)
            {
                MessageBox.Show("Choose card name from list!");
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
