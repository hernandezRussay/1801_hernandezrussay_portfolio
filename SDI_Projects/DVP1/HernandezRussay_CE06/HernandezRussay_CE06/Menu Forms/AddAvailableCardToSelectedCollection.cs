﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CollectionManagerApp
{
    public partial class AddAvailableCardToSelectedCollection : Form
    {
        /// <summary>
        /// Selected collection Name
        /// </summary>
        public string SelectedCollection { get; private set; }

        /// <summary>
        /// Created card obj
        /// </summary>
        public Card Card { get; private set; }

        /// <summary>
        /// collections manager reference
        /// </summary>
        private CollectionsManager collectionsManager;

        public AddAvailableCardToSelectedCollection(CollectionsManager collectionsManager)
        {
            InitializeComponent();
            this.collectionsManager = collectionsManager;
            this.DialogResult = DialogResult.Cancel;
        }

        private void SelectCollectionForm_Load(object sender, EventArgs e)
        {
            // load all collection names
            collectionCbox.Items.Clear();
            for (int i = 0; i < this.collectionsManager.allCollections.Count; ++i)
            {
                collectionCbox.Items.Add(this.collectionsManager.allCollections.Keys.ElementAt(i));
            }

            // load all card names
            cardsCbox.Items.Clear();
            for (int i = 0; i < this.collectionsManager.availableCards.Count; ++i)
            {
                cardsCbox.Items.Add(this.collectionsManager.availableCards[i].Name);
            }
        }

        private void SubmitBtn_Click(object sender, EventArgs e)
        {
            // check input data
            if (collectionCbox.SelectedIndex == -1)
            {
                MessageBox.Show("Choose collection name from list!");
                return;
            }

            if (cardsCbox.SelectedIndex == -1)
            {
                MessageBox.Show("Choose card name from list of available cards!");
                return;
            }

            this.SelectedCollection = collectionCbox.Items[collectionCbox.SelectedIndex].ToString();
            this.Card = this.collectionsManager.availableCards[cardsCbox.SelectedIndex];
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
