﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CollectionManagerApp
{
    public partial class SelectCollectionForm : Form
    {
        /// <summary>
        /// Selected collection Name
        /// </summary>
        public string SelectedCollection { get; private set; }

        /// <summary>
        /// collections manager reference
        /// </summary>
        private CollectionsManager collectionsManager;

        public SelectCollectionForm(CollectionsManager collectionsManager)
        {
            InitializeComponent();
            this.collectionsManager = collectionsManager;
            this.DialogResult = DialogResult.Cancel;
        }

        private void SelectCollectionForm_Load(object sender, EventArgs e)
        {
            // load all collection names
            collectionCbox.Items.Clear();
            for (int i = 0; i < this.collectionsManager.allCollections.Count; ++i)
            {
                collectionCbox.Items.Add(this.collectionsManager.allCollections.Keys.ElementAt(i));
            }
        }

        private void SubmitBtn_Click(object sender, EventArgs e)
        {
            // if user selected collection name
            if (collectionCbox.SelectedIndex > -1)
            {
                this.SelectedCollection = collectionCbox.Items[collectionCbox.SelectedIndex].ToString();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Choose collection name from list!");
            }
        }


    }
}
