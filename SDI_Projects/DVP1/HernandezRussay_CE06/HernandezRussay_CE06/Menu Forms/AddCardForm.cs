﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CollectionManagerApp
{
    public partial class AddCardForm : Form
    {
        /// <summary>
        /// Created card obj
        /// </summary>
        public Card CreatedCard { get; private set; }

        /// <summary>
        /// ctor
        /// </summary>
        public AddCardForm()
        {
            InitializeComponent();
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Check input data fields
        /// </summary>
        private bool CheckInputData()
        {
            if (cardNameTbx.Text.Trim().Length == 0)
            {
                MessageBox.Show("Card's name field can't be empty!");
                return false;
            }

            if (cardDescriptionTbx.Text.Trim().Length == 0)
            {
                MessageBox.Show("Card's description field can't be empty!");
                return false;
            }

            if (value_nuD.Value == 0)
            {
                MessageBox.Show("Card's value must be greater than zero!");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Submit button event handler
        /// </summary>
        private void AddCardBtn_Click(object sender, EventArgs e)
        {
            // check input data fields
            bool result = CheckInputData();

            // if it's ok
            if (result)
            {
                // creating new card
                this.CreatedCard = new Card(cardNameTbx.Text.Trim(), cardDescriptionTbx.Text.Trim(), value_nuD.Value);

                // say that dialogresult is ok and close form
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}
