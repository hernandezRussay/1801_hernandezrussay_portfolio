﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CollectionManagerApp
{
    public partial class AddCollectionForm : Form
    {
        /// <summary>
        /// new collection name
        /// </summary>
        public string CollectionName { get; private set; }

        /// <summary>
        /// collections manager reference
        /// </summary>
        private CollectionsManager collectionsManager;
        
        /// <summary>
        /// ctor which get collections manager obj
        /// </summary>
        public AddCollectionForm(CollectionsManager collectionsManager)
        {
            InitializeComponent();
            this.collectionsManager = collectionsManager;
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// when user input collection name (event handler)
        /// </summary>
        private void collectionNameTbx_TextChanged(object sender, EventArgs e)
        {
            // input by user collection Name
            string collectionName = collectionNameTbx.Text;
            // check if collection manager contain collection with entered name exist
            if (this.collectionsManager.allCollections.ContainsKey(collectionName))
            {
                // change text color to red
                collectionNameTbx.ForeColor = Color.Red;
            }
            else
            {
                // change text color to green
                collectionNameTbx.ForeColor = Color.Green;
            }
        }


        private void AddCollectionBtn_Click(object sender, EventArgs e)
        {
            // get collection name from textbox
            string collectionName = collectionNameTbx.Text.Trim();

            // check input data
            if (collectionName.Length == 0)
            {
                MessageBox.Show("collection's name field can't be empty!");
                return;
            }

            // check if collection manager contain collectionName in dictionary
            if (this.collectionsManager.allCollections.ContainsKey(collectionName))
            {
                MessageBox.Show("Collection with the same name already exist in Dictionary!\r\nChoose another name, please!");
                return;
            }

            this.CollectionName = collectionName;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
