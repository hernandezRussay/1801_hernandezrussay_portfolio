﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CollectionManagerApp
{
    public partial class MoveCardBetweenCollectionsForm : Form
    {
        /// <summary>
        /// first collection Name
        /// </summary>
        public string FirstCollection { get; private set; }

        /// <summary>
        /// second collection Name
        /// </summary>
        public string SecondCollection { get; private set; }

        /// <summary>
        /// Created card obj
        /// </summary>
        public Card Card { get; private set; }

        /// <summary>
        /// collections manager reference
        /// </summary>
        private CollectionsManager collectionsManager;

        public MoveCardBetweenCollectionsForm(CollectionsManager collectionsManager)
        {
            InitializeComponent();
            this.collectionsManager = collectionsManager;
            this.DialogResult = DialogResult.Cancel;
        }

        private void SelectCollectionForm_Load(object sender, EventArgs e)
        {
            // load all collection names
            collectionCbox.Items.Clear();
            for (int i = 0; i < this.collectionsManager.allCollections.Count; ++i)
            {
                string collectionName = this.collectionsManager.allCollections.Keys.ElementAt(i);
                collectionCbox.Items.Add(collectionName);
                secondCollectionCbx.Items.Add(collectionName);
            }
        }

        private void collectionCbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (collectionCbox.SelectedIndex > -1)
            {
                // load all cards in selected collection
                cardsCbox.Items.Clear();

                this.FirstCollection = collectionCbox.Items[collectionCbox.SelectedIndex].ToString();
                List<Card> cardsInCollection = this.collectionsManager.allCollections[this.FirstCollection];

                for (int i = 0; i < cardsInCollection.Count; ++i)
                {
                    cardsCbox.Items.Add(cardsInCollection[i].Name);
                }
            }
        }

        private void secondCollectionCbx_SelectedIndexChanged(object sender, EventArgs e)
        {   
            if (collectionCbox.SelectedIndex > -1)
            {
                // get second collection name if it's chosen
                this.SecondCollection = secondCollectionCbx.Items[secondCollectionCbx.SelectedIndex].ToString();
            }
        }

        private void cardsCbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cardsCbox.SelectedIndex > -1)
            {
                // get selected collection
                List<Card> cardsInCollection = this.collectionsManager.allCollections[this.FirstCollection];
                // get card name
                string cardName = cardsCbox.Items[cardsCbox.SelectedIndex].ToString();
                // search card in selected collection
                this.Card = Program.SearchCardInListByName(cardName, cardsInCollection);
            }
        }

        private void SubmitBtn_Click(object sender, EventArgs e)
        {
            // check input data
            if (string.IsNullOrEmpty(this.FirstCollection))
            {
                MessageBox.Show("Choose first collection name from list!");
                return;
            }

            if (string.IsNullOrEmpty(this.SecondCollection))
            {
                MessageBox.Show("Choose second collection name from list!");
                return;
            }

            if (this.FirstCollection == this.SecondCollection)
            {
                MessageBox.Show("You have no need to move one card in one collection!!!\r\nChoose different collections!");
                return;
            }

            if (this.Card == null)
            {
                MessageBox.Show("Choose card name from list!");
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
