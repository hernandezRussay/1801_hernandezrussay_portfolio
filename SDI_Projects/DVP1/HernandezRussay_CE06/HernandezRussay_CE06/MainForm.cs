﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CollectionManagerApp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// our main variable with collections manager
        /// </summary>
        private CollectionsManager currentCollectionsManager;

        #region menu event handlers

        /// <summary>
        /// Create collection manager menu event handler
        /// </summary>
        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.currentCollectionsManager == null)
            {
                // creating collection managers
                this.currentCollectionsManager = new CollectionsManager();

                // enable collections and cards menu items when collection manager was created!
                collectionsToolStripMenuItem.Enabled = true;
                cardsToolStripMenuItem.Enabled = true;

                createToolStripMenuItem.Text = "Recreate";

                MessageBox.Show("Collection manager created!", "Caution");
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure? (all created collections and cards will be removed!!!)", "Recreate collections manager", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes) // if user pressed Yes
                {
                    // recreate collection manager object
                    this.currentCollectionsManager = new CollectionsManager();

                    // reset all controls
                    collectionNameTbx.ResetText();
                    descriptionTbx.ResetText();
                    valueTbx.ResetText();
                    cardsInCollectionlistBox.Items.Clear();
                    availableCardDescriptionTbx.ResetText();
                    availableCardValueTbx.ResetText();
                    availableCardslistBox.Items.Clear();

                    MessageBox.Show("Collection manager was recreated!\r\nPlease kindly note, that all collections and cards was removed!", "Caution");
                }
            }
        }
        /// <summary>
        /// Menu exit event handler
        /// </summary>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // we ask user if he really want to exit?
            DialogResult dialogResult = MessageBox.Show("Are you sure?", "Exit program", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes) // if user pressed Yes
            {
                Application.Exit();
            }
        }
        /// <summary>
        /// Create card (event handler)
        /// </summary>
        private void createACardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // creating add card form
            AddCardForm addCardForm = new AddCardForm();
            // show dialog and wait till it close
            DialogResult dialogResult = addCardForm.ShowDialog();
            // if everything is ok
            if (dialogResult == DialogResult.OK)
            {
                // get created card
                Card createdCard = addCardForm.CreatedCard;

                // add it to collections manager
                this.currentCollectionsManager.availableCards.Add(createdCard);

                // Update availableCardslistBox
                UpdateAvailableCards();

                MessageBox.Show("New card added!");
            }
            else
            {
                MessageBox.Show("Card not added!");
            }
        }
        /// <summary>
        /// create collection (event handler)
        /// </summary>
        private void createCollectionToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            // creating add Collection form
            AddCollectionForm addCollectionForm = new AddCollectionForm(this.currentCollectionsManager);
            // show dialog and wait till it close
            DialogResult dialogResult = addCollectionForm.ShowDialog();
            // if everything is ok
            if (dialogResult == DialogResult.OK)
            {
                // get new collection name
                string newCollectionName = addCollectionForm.CollectionName;

                // add it to collections manager
                this.currentCollectionsManager.allCollections.Add(newCollectionName, new List<Card>());

                MessageBox.Show("New collection added!");
            }
            else
            {
                MessageBox.Show("Collection not added!");
            }
        }
        /// <summary>
        /// Display all collection cards in listbox
        /// </summary>
        private void displayAllCollectionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /// reset controls
            cardsInCollectionlistBox.Items.Clear();
            collectionNameTbx.ResetText();
            valueTbx.ResetText();
            descriptionTbx.ResetText();

            // get collections number
            int collectionsNumber = this.currentCollectionsManager.allCollections.Count;
            // go through the allCollections dictionary
            for (int i = 0; i < collectionsNumber; ++i)
            {
                // get collection name
                string collectionName = this.currentCollectionsManager.allCollections.ElementAt(i).Key;
                // get list of cards in collection
                List<Card> cardsInCollection = this.currentCollectionsManager.allCollections.ElementAt(i).Value;
                int cardsCount = this.currentCollectionsManager.allCollections.ElementAt(i).Value.Count;
                // go through the list of cards in collection
                for (int j = 0; j < cardsCount; ++j)
                {
                    // get card name from the list
                    string cardName = cardsInCollection[j].Name;
                    string result = string.Format("{0}:{1}", collectionName, cardName);
                    // add it into listbox
                    cardsInCollectionlistBox.Items.Add(result);
                }
            }

            if (collectionsNumber > 0)
            {
                MessageBox.Show("All collections displayed!");
            }
            else
            {
                MessageBox.Show("There are no collections in collection manager yet!");
            }
        }
        /// <summary>
        /// Display cards from selected collection 
        /// </summary>
        private void displaySelectedCollectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // creating select collection form
            SelectCollectionForm selectCollectionForm = new SelectCollectionForm(this.currentCollectionsManager);
            // show dialog and wait till it close
            DialogResult dialogResult = selectCollectionForm.ShowDialog();
            // if everything is ok
            if (dialogResult == DialogResult.OK)
            {
                // display cards in selected collection
                string selectedCollectionName = selectCollectionForm.SelectedCollection;
                ShowCollectionCards(selectedCollectionName);

                MessageBox.Show("Cards displayed!");
            }
            else
            {
                MessageBox.Show("Cards not displayed!");
            }
        }

        /// <summary>
        /// Add card to selected collection
        /// </summary>
        private void addSelectedCardToSelectedCollectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // creating add AddAvailableCardToSelectedCollection
            AddAvailableCardToSelectedCollection addAvailableCardToSelectedCollection =
                new AddAvailableCardToSelectedCollection(this.currentCollectionsManager);
            // show dialog and wait till it close
            DialogResult dialogResult = addAvailableCardToSelectedCollection.ShowDialog();
            // if everything is ok
            if (dialogResult == DialogResult.OK)
            {
                // select card and collection data 
                string selectedCollectionName = addAvailableCardToSelectedCollection.SelectedCollection;
                Card selectedCard = addAvailableCardToSelectedCollection.Card;

                // add card to collection
                this.currentCollectionsManager.allCollections[selectedCollectionName].Add(selectedCard);
                // remove card from available cards list
                this.currentCollectionsManager.availableCards.Remove(selectedCard);

                UpdateAvailableCards();

                MessageBox.Show(string.Format("Card {0} added to {1} collection!", selectedCard.Name, selectedCollectionName));
            }
            else
            {
                MessageBox.Show("Card not added!");
            }
        }

        /// <summary>
        /// remove selected card from selected collection (event handler)
        /// </summary>
        private void removeSelectedCardFromSelectedCollectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // creating RemoveSelectedCardCollectionForm
            RemoveSelectedCardCollectionForm removeSelectedCardCollectionForm =
                new RemoveSelectedCardCollectionForm(this.currentCollectionsManager);
            // show dialog and wait till it close
            DialogResult dialogResult = removeSelectedCardCollectionForm.ShowDialog();
            // if everything is ok
            if (dialogResult == DialogResult.OK)
            {
                // select card and collection data 
                string selectedCollectionName = removeSelectedCardCollectionForm.SelectedCollection;
                Card selectedCard = removeSelectedCardCollectionForm.Card;

                // remove card from collection
                this.currentCollectionsManager.allCollections[selectedCollectionName].Remove(selectedCard);
                // add to available cards list
                this.currentCollectionsManager.availableCards.Add(selectedCard);

                UpdateAvailableCards();

                MessageBox.Show(string.Format("Card {0} removed from {1} collection!", selectedCard.Name, selectedCollectionName));
            }
            else
            {
                MessageBox.Show("Card not removed!");
            }
        }
        /// <summary>
        /// Move card between collections (event handler)
        /// </summary>
        private void moveCardBetweenCollectionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // creating MoveCardBetweenCollectionsForm
            MoveCardBetweenCollectionsForm moveCardBetweenCollectionsForm =
                new MoveCardBetweenCollectionsForm(this.currentCollectionsManager);
            // show dialog and wait till it close
            DialogResult dialogResult = moveCardBetweenCollectionsForm.ShowDialog();
            // if everything is ok
            if (dialogResult == DialogResult.OK)
            {
                // select card and collection data 
                string firstCollectionName = moveCardBetweenCollectionsForm.FirstCollection;
                Card selectedCard = moveCardBetweenCollectionsForm.Card;
                string secondCollectionName = moveCardBetweenCollectionsForm.SecondCollection;
                // remove card from first collection
                this.currentCollectionsManager.allCollections[firstCollectionName].Remove(selectedCard);
                // add card to second collection
                this.currentCollectionsManager.allCollections[secondCollectionName].Add(selectedCard);


                MessageBox.Show(string.Format("Card {0} was successfully moved from {1} collection to {2} collection!", selectedCard.Name, firstCollectionName, secondCollectionName));
            }
            else
            {
                MessageBox.Show("Operation canceled!");
            }
        }
        #endregion

        #region other event handlers
        /// <summary>
        /// When user choose element in availableCardslistBox
        /// </summary>
        private void availableCardslistBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // get selected index property
            int selectedIndex = availableCardslistBox.SelectedIndex;
            // if some item was selected
            if (selectedIndex > -1)
            {
                // search card in collection manager by Name field
                string cardName = availableCardslistBox.Items[selectedIndex].ToString();
                Card card = Program.SearchCardInListByName(cardName, this.currentCollectionsManager.availableCards);

                if (card != null)
                {
                    // output description and value of selected card
                    availableCardDescriptionTbx.Text = card.Description;
                    availableCardValueTbx.Text = card.Value.ToString();
                }
                else
                {
                    MessageBox.Show("Card not found in collection manager!", "Caution");
                }
            }
        }
        /// <summary>
        /// When user choose element cardsInCollectionlistBox
        /// </summary>
        private void cardsInCollectionlistBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // get selected index property
            int selectedIndex = cardsInCollectionlistBox.SelectedIndex;
            // if some item was selected
            if (selectedIndex > -1)
            {
                // search card in collection manager by Name field
                string cardName = cardsInCollectionlistBox.Items[selectedIndex].ToString();

                Card card = null;

                // in case user displayed all collections
                string[] Parts = cardName.Split(':');
                if (Parts.Length == 2)
                {
                    collectionNameTbx.Text = Parts[0];
                    card = Program.SearchCardInListByName(Parts[1], this.currentCollectionsManager.allCollections[Parts[0]]);
                }
                else
                {
                    card = Program.SearchCardInListByName(cardName, this.currentCollectionsManager.allCollections[collectionNameTbx.Text]);
                }

                if (card != null)
                {
                    // output description and value of selected card
                    descriptionTbx.Text = card.Description;
                    valueTbx.Text = card.Value.ToString();
                }
                else
                {
                    MessageBox.Show("Card not found in collection manager!", "Caution");
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Update availableCards ListBox
        /// </summary>
        private void UpdateAvailableCards()
        {
            if (this.currentCollectionsManager != null)
            {
                // reset controls
                availableCardslistBox.Items.Clear();
                availableCardDescriptionTbx.ResetText();
                availableCardValueTbx.ResetText();

                // get available cards number
                int availableCardsNumber = this.currentCollectionsManager.availableCards.Count;
                // go through the availableCards list
                for (int i = 0; i < availableCardsNumber; ++i)
                {
                    // get card name from the list
                    string cardName = this.currentCollectionsManager.availableCards[i].Name;
                    // add it into listbox
                    availableCardslistBox.Items.Add(cardName);
                }
            }
        }
        /// <summary>
        /// Show collections in cardsInCollectionlistBox
        /// </summary>
        private void ShowCollectionCards(string collectionName)
        {
            if (this.currentCollectionsManager != null)
            {
                collectionNameTbx.Text = collectionName;
                cardsInCollectionlistBox.Items.Clear();

                List<Card> cardsInCollection = this.currentCollectionsManager.allCollections[collectionName];
                for (int i = 0; i < cardsInCollection.Count; ++i)
                {
                    string cardName = cardsInCollection[i].Name;
                    cardsInCollectionlistBox.Items.Add(cardName);
                }
            }
        }
        #endregion
    }
}
