﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hernandez_russay_FinalProject
{
    class Program
    {
        static void Main(string[] args)
        {
            //Russay Hernandez
            //December 16th, 2017
            //final project

            Console.WriteLine("Number of trips you wish to take: ");
            string numTrip = Console.ReadLine();
            int numTrip2;
            while (!int.TryParse(numTrip, out numTrip2))
            {
                Console.WriteLine("You typed something other than a number. Enter the number of trips: ");
                numTrip = Console.ReadLine();
            }


            double[] arrayTrip = GetMiles(numTrip2);   //pushed the array to the get miles function



            double totalAllMiles = TotalMiles(arrayTrip);

            Console.WriteLine("Miles per gallon of your car: ");
            string mPG = Console.ReadLine();
            double mPGDouble;
            while (!double.TryParse(mPG, out mPGDouble))
            {
                Console.WriteLine("You didn't type a number. Enter miles per gallon: ");
                mPG = Console.ReadLine();
            }

            Console.WriteLine("Cost per gallon of gas: ");
            string costPerGal = Console.ReadLine();
            double costPerGal1;
            while (!double.TryParse(costPerGal, out costPerGal1))
            {
                Console.WriteLine("You entered something that's not a number. Please enter dollars per gallon: ");
                costPerGal = Console.ReadLine();
            }


            double totalCost = CostOfGas(totalAllMiles, mPGDouble, costPerGal1);



            Console.WriteLine("The total cost of driving {0} miles is {1}.", totalAllMiles, totalCost.ToString("C"));

            /*
            Data Sets To Test:
            1. 
            i. # of trips: 3
            ii. 1st trip = 200, 2nd trip = 100, 3rd trip = 30
            iii. MPG = 30
            iv. Cost of 1 gallon of gas is $2.20
            v. Final Output: "The total cost of driving 330 miles is $24.20."
            2.
            i. # of trips: 2
            ii. 1st trip = 500, 2nd trip = 50
            iii. MPG = 20
            iv. Cost of 1 gallon of gas is $3.00
            v. Final Output: "The total cost of driving 550 miles is $82.50."
            3.
            i. # of trips: 3
            ii. 1st trip = 30, 2nd trip = 40, 3rd trip = 60
            iii. MPG = 25
            iv. Cost of 1 gallon of gas is $1.00
            v. Final Output: "The total cost of driving 130 miles is $5.20."
            */

        }

        /*first function getMiles*/


        public static double[] GetMiles(int numtrip2) // first function
        {
            
            double[] array1 = new double[numtrip2]; //generated a double array
           
            for (int i = 1; i < (numtrip2 + 1); i++)
            {
                Console.WriteLine("Number of miles for trip #{0}", i);
                string thisTrip = Console.ReadLine();
                double thisTrip1;
                while (!double.TryParse(thisTrip, out thisTrip1))
                {
                    Console.WriteLine("You entered something other than a number. Enter the miles for the trip: ");
                    thisTrip = Console.ReadLine();
                }
                array1[(i - 1)] = thisTrip1;
            }
            return array1;

        }

        /*second function totalMiles*/
        public static double TotalMiles(double[] received)//recieved the other array and sent the info to a new one
        {
            double totalMiles = 0;
            

            for (int i = 0; i < received.Length; i++)            //created a for loop to cycle through the array 
            {
                totalMiles += received[i];//added the total mils to each item
            }

            return totalMiles; // added that result to the array


        }


        public static double CostOfGas(double total, double mPG, double gasCost)
        {

            double price = (total / mPG) * gasCost;   // used some math the figure out the cost of gas
            return price;        

        }

    }
}
