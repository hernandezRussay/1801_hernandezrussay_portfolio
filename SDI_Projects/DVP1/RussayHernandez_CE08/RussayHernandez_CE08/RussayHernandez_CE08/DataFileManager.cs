﻿using System;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// Russay Hernandez
/// 4/22/2018
/// MDV2430-O
/// </summary>

namespace RussayHernandez_CE08
{
    public class DataFileManager
    {
        // DATA FIELDS LAYOUT CONSTANTS
        public const int NUMBER_OF_FIELDS_IN_RECORD = 150;
        public const string DATA_FIELDS_LAYOUT_FILEPATH = @"\Data\Layout\DataFieldsLayout.txt";

        // DATA FILE FILEPATH CONSTANTS
        public const string DATA_FILE1_FILEPATH = @"\Data\DataFile1.txt";
        public const string DATA_FILE2_FILEPATH = @"\Data\DataFile2.txt";
        public const string DATA_FILE3_FILEPATH = @"\Data\DataFile3.txt";


        /// <summary>
        /// Filepath to DataFieldsLayout.txt (public because of readonly modifier)
        /// </summary>
        public readonly string FullFilePathToDataFieldsLayout;

        /// <summary>
        /// data fields layout
        /// </summary>
        private string[] fieldNames;

        /// <summary>
        /// Header record
        /// </summary>
        private HeaderStruct headerRecord;
        /// <summary>
        /// list of records(with pair:key dictionary)
        /// </summary>
        private List<Dictionary<string, string>> fileRecords;
        /// <summary>
        /// Trailer record
        /// </summary>
        private HeaderStruct trailerRecord;

        /// <summary>
        /// Ctor
        /// </summary>
        public DataFileManager()
        {
            this.FullFilePathToDataFieldsLayout = Directory.GetCurrentDirectory() + DATA_FIELDS_LAYOUT_FILEPATH;
            this.fileRecords = new List<Dictionary<string, string>>();
        }

        /// <summary>
        /// Reading file with data fields layout
        /// </summary>
        public bool PrepareDataFieldsLayout(ref string err)
        {
            try
            {
                // using stream reader for reading headers in file 
                using (StreamReader streamReader = new StreamReader(this.FullFilePathToDataFieldsLayout))
                {
                    List<string> dataFieldsLayoutList = new List<string>(NUMBER_OF_FIELDS_IN_RECORD);

                    // while  stream position is not at the end
                    while (!streamReader.EndOfStream)
                    {
                        // read field name
                        string fieldName = streamReader.ReadLine();
                        // add it to list with data field names
                        dataFieldsLayoutList.Add(fieldName);
                    }

                    // close stream
                    streamReader.Close();

                    // save field names in arrays
                    this.fieldNames = dataFieldsLayoutList.ToArray();

                    // return result operation
                    return true;
                }
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Preparing header or trailer object
        /// </summary>
        private bool PrepareHeaderObject(string headerLine, ref HeaderStruct header, ref string err, string txt)
        {
            // split header by space symbol
            string[] headerParts = headerLine.Split(' ');

            try
            {
                // preparing header object
                header = new HeaderStruct(headerParts[0], headerParts[1], headerParts[2],
                                          headerParts[3], headerParts[4], headerParts[5]);
            }
            catch
            {
                err = txt;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Reading data file
        /// </summary>
        public bool ReadDataFile(string filePath, ref string err)
        {
            try
            {
                // using stream reader for reading headers in file 
                using (StreamReader streamReader = new StreamReader(filePath))
                {
                    // read first line
                    string Line = streamReader.ReadLine();

                    // check header of data file
                    if (!Line.StartsWith("BOF"))
                    {
                        err = "Wrong file format!";
                        return false;
                    }
                    else
                    {
                        // preparing header record
                        if (!PrepareHeaderObject(Line, ref this.headerRecord, ref err, "Wrong header format!"))
                            return false;
                    }

                    // check trailer format
                    bool TrailerExist = false;

                    // while  stream position is not at the end
                    while (!streamReader.EndOfStream)
                    {
                        // read record
                        Line = streamReader.ReadLine();

                        // if it is trailer record
                        if (Line.StartsWith("EOF"))
                        {
                            TrailerExist = true;

                            // preparing trailer record
                            if (!PrepareHeaderObject(Line, ref this.trailerRecord, ref err, "Wrong trailer format!"))
                                return false;
                        }
                        else
                        {
                            // split fields in record
                            string[] fieldsInRecord = Line.Split('|');

                            // if bad number of fields
                            if (fieldsInRecord.Length != NUMBER_OF_FIELDS_IN_RECORD)
                            {
                                err = string.Format("Wrong fields number: {0} (must be {1})", 
                                                    fieldsInRecord.Length, NUMBER_OF_FIELDS_IN_RECORD);
                                return false;
                            }

                            // record dictionary with fields data
                            Dictionary<string, string> record = new Dictionary<string, string>(NUMBER_OF_FIELDS_IN_RECORD);

                            // fill record dictionary
                            for (int i = 0; i < fieldsInRecord.Length; ++i)
                            {
                                record.Add(this.fieldNames[i], fieldsInRecord[i]);
                            }

                            // add it to file records
                            this.fileRecords.Add(record);
                        }
                    }

                    if (!TrailerExist)
                    {
                        err = "Trailer doesn't exist!";
                        return false;
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Convert dictionary data to json string
        /// </summary>
        private string DictionaryToJsonString(Dictionary<string, string> record)
        {
            // create first brace
            string jsonRecord = "{ ";

            // add fields
            foreach (KeyValuePair<string, string> pair in record)
            {
                jsonRecord += string.Format("\"{0}\" : \"{1}\" , ", pair.Key, pair.Value);
            }

            // remove last comma
            jsonRecord = jsonRecord.Remove(jsonRecord.Length - 2);
            // add last brace
            jsonRecord += " }";

            return jsonRecord;
        }
        /// <summary>
        /// Convert all file data to json string
        /// </summary>
        private string FileDataToJsonString(List<Dictionary<string, string>> fileData)
        {
            // add first bracker
            string jsonFileData = "[ ";
            
            // add all json records
            for (int i = 0; i < fileData.Count; ++i)
            {
                // get json record from dictionary
                string jsonRecord = DictionaryToJsonString(fileData[i]);
                // add it to jsonFileData
                jsonFileData += string.Format("{0} , ", jsonRecord);
            }

            // remove last comma
            jsonFileData = jsonFileData.Remove(jsonFileData.Length - 2);
            // add last bracket
            jsonFileData += " ]";

            return jsonFileData;
        }

        /// <summary>
        /// Convert read file to json string
        /// </summary>
        public string ConvertReadFileToJson()
        {
            // Header json element
            string headerJsonStr = string.Format("\"{0}\" : {1}", "Header", this.headerRecord.ToJsonString());
            // Data json elements
            string fileDataJsonStr = string.Format("\"{0}\" : {1}", "Data", FileDataToJsonString(this.fileRecords));
            // Trailer json element
            string trailerJsonStr = string.Format("\"{0}\" : {1}", "Trailer", this.trailerRecord.ToJsonString());

            // Make whole json file string
            string JSON_DATA = string.Format("{0} {1}, {2}, {3} {4}", "{", headerJsonStr, fileDataJsonStr, trailerJsonStr, "}");

            return JSON_DATA;
        }
    }
}
