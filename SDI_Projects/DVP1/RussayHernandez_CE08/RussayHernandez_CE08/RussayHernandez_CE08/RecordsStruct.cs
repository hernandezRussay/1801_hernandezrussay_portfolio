﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Russay Hernandez
/// 4/22/2018
/// MDV2430-O
/// </summary>

namespace RussayHernandez_CE08
{
    /// <summary>
    /// Header record struct
    /// </summary>
    public class HeaderStruct
    {
        /// <summary>
        /// Header Name
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Header modifier
        /// </summary>
        public string Modifier { get; private set; }
        /// <summary>
        /// Header first date
        /// </summary>
        public string FirstDate { get; private set; }
        /// <summary>
        /// Header last date
        /// </summary>
        public string LastDate { get; private set; }
        /// <summary>
        /// Header record count always zeros
        /// </summary>
        public string RecordCountAlwaysZeros { get; private set; }
        /// <summary>
        /// header sequence number
        /// </summary>
        public string SequenceNumber { get; private set; }

        /// <summary>
        /// Ctor
        /// </summary>
        public HeaderStruct(string name, string modifier, string firstDate, 
                            string lastDate, string recordCountAlwaysZeros, string sequenceNumber)
        {
            this.Name = name;
            this.Modifier = modifier;
            this.FirstDate = firstDate;
            this.LastDate = lastDate;
            this.RecordCountAlwaysZeros = recordCountAlwaysZeros;
            this.SequenceNumber = sequenceNumber;
        }

        /// <summary>
        /// Converts object data to JSON string
        /// </summary>
        public string ToJsonString()
        {
            string jsonHeader = "{ ";
            jsonHeader += string.Format("\"Name\" : \"{0}\", \"Modifier\" : \"{1}\", \"FirstDate\" : \"{2}\", \"LastDate\" : \"{3}\", \"RecordCountAlwaysZeros\" : \"{4}\", \"SequenceNumber\" : \"{5}\"", 
                                       this.Name, this.Modifier, this.FirstDate, this.LastDate, this.RecordCountAlwaysZeros, this.SequenceNumber);
            jsonHeader += " }";
            return jsonHeader;
        }
    }
}
