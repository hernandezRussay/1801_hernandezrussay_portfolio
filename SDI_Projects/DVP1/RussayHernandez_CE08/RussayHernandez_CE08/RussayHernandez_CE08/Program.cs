﻿using System;
using System.IO;
using System.Collections.Generic;

/// <summary>
/// Russay Hernandez
/// 4/22/2018
/// MDV2430-O
/// </summary>

namespace RussayHernandez_CE08
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to JSON PARSER!");

            // creating data file manager
            DataFileManager dataFileManager = new DataFileManager();

            #region PREPARING DATA
            Console.WriteLine("\r\nPreparing necesary data... ");

            // err variable
            string err = string.Empty;
            // preparing datafieldslayout.txt
            bool preparingResult = dataFileManager.PrepareDataFieldsLayout(ref err);

            // if there's an error when file was read
            if (!preparingResult)
            {
                Console.WriteLine("An error occured while preparing data!\r\nfile({0})\r\nMessage: {1}\r\n", 
                                  dataFileManager.FullFilePathToDataFieldsLayout, err);
                Console.WriteLine("Press any key to exit ...");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Data successfully prepared!");
            #endregion
            
            // key code to close program
            ConsoleKey exitKey;

            do
            {
                Console.WriteLine("\r\nChoose one of the following files you want to convert to JSON format (Press 1, 2, 3):\r\n 1. {0}\r\n 2. {1}\r\n 3. {2}\r\n",
                                  DataFileManager.DATA_FILE1_FILEPATH, DataFileManager.DATA_FILE2_FILEPATH, DataFileManager.DATA_FILE3_FILEPATH);

                // read line from console
                string userInput = Console.ReadLine();

                // try to parse user input to int
                int variant;
                if (int.TryParse(userInput, out variant))
                {
                    // if user input values 1, 2 or 3
                    if (variant > 0 && variant < 4)
                    {
                        // preparing data file path
                        string fullFilePathToDataFile = Directory.GetCurrentDirectory();

                        switch (variant)
                        {
                            case 1: fullFilePathToDataFile += DataFileManager.DATA_FILE1_FILEPATH; break;
                            case 2: fullFilePathToDataFile += DataFileManager.DATA_FILE2_FILEPATH; break;
                            case 3: fullFilePathToDataFile += DataFileManager.DATA_FILE3_FILEPATH; break;
                        }

                        // read file
                        bool readingResult = dataFileManager.ReadDataFile(fullFilePathToDataFile, ref err);

                        if (readingResult)
                        {
                            Console.WriteLine("File({0}) successfully read!\r\n", fullFilePathToDataFile);

                            // Convering to JSON
                            string JSON_DATA = dataFileManager.ConvertReadFileToJson();

                            // make output path
                            string outputPath = Directory.GetCurrentDirectory() + @"\Json output";
                            if (!Directory.Exists(outputPath))
                            {
                                Directory.CreateDirectory(outputPath);
                            }
                            string jsonFileName = string.Format(@"\jsonDataFile_{0}.json", DateTime.Now.ToString("dd_MM_yy_HH_mm_ss"));
                            outputPath += jsonFileName;

                            // save file
                            File.WriteAllText(outputPath, JSON_DATA);

                            Console.WriteLine("File \"{0}\" successfully converted to json format.\r\nNew file name \"{1}\" in \"Json output\" directory", fullFilePathToDataFile, jsonFileName);
                        }
                        else
                        {
                            Console.WriteLine("Reading file({0}) failed!\r\nMessage: {1}", fullFilePathToDataFile, err);
                        }
                    }
                    else
                    {
                        Console.WriteLine("You have only 3 variants!");
                    }
                }
                else
                {
                    Console.WriteLine("Incorrect input!");
                }

                Console.WriteLine("\r\nPress \"Y\" if you want to exit... ");
                // handle user input key
                ConsoleKeyInfo consoleKeyInfo = Console.ReadKey();
                exitKey = consoleKeyInfo.Key;

            } while (exitKey != ConsoleKey.Y);

        }
    }
}
