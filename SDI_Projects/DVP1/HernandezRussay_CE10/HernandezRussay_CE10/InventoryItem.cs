﻿namespace HernandezRussay_CE10
{
    public class InventoryItem
    {
        private string name;
        private string style;
        private decimal price;


        private InventoryItem()
        {

        }
        public InventoryItem(string name, string style, decimal price) : this()
        {
            this.name = name;
            this.price = price;
            this.style = style;
        }

        
        public string GetName()
        {
            return name;
        }
        public decimal GetPrice()
        {
            return price;
        }
        public string GetStyle()
        {
            return style;
        }

        public override string ToString()
        {
            return string.Format("{0} - {1} for {2:0.##}", name, style, price);
        }
    }
}
