﻿using System.Collections.Generic;

namespace HernandezRussay_CE10
{
    public class Customer
    {
        private string name;
        private List<InventoryItem> shoppingCart;

        public Customer()
        {
            shoppingCart = new List<InventoryItem>();
        }

        public Customer(string name) : this()
        {
            this.name = name;
        }

        public void AddInventoryItem(InventoryItem item)
        {
            shoppingCart.Add(item);
        }
        public void RemoveInventoryItem(InventoryItem item)
        {
            shoppingCart.Remove(item);
        }
        public List<InventoryItem> GetShoppingCart()
        {
            return shoppingCart;
        }

        public override string ToString()
        {
            return name;
        }
    }
}
