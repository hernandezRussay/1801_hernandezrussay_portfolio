﻿namespace HernandezRussay_CE10
{
    public interface ILog
    {
        /// <summary>
        /// returns nothing and takes a string parameter
        /// </summary>
        /// <param name="message"></param>
        void Log(string message);
        /// <summary>
        /// LogD - returns nothing and takes a string parameter
        /// </summary>
        /// <param name="message"></param>
        void LogD(string message);
        /// <summary>
        /// LogW - returns nothing and takes a string parameter
        /// </summary>
        /// <param name="message"></param>
        void LogW(string message);
    }
}
