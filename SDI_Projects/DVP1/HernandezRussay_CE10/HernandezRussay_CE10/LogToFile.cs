﻿using System;
using System.IO;

namespace HernandezRussay_CE10
{
    class LogToFile : Logger
    {
        private const string logFile = "log.txt";
        private static int line = 1;

        public LogToFile()
        {
            if (File.Exists(logFile))
                File.Delete(logFile);

            using (var stream = File.Create(logFile)) ;

        }

        public override void Log(string message)
        {
            File.AppendAllLines(logFile, new string[] { string.Format("{0}: {1}", line++, message) });
        }

        public override void LogD(string message)
        {
            throw new NotImplementedException();
        }

        public override void LogW(string message)
        {

        }
    }
}
