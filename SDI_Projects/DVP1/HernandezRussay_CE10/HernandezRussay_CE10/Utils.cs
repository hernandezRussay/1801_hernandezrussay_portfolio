﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HernandezRussay_CE10
{
    public static class Utils
    {
        public static readonly Dictionary<int, string> MainMenuItems = new Dictionary<int, string>()
        {
            { 1, "Select current shopper" },
            { 2, "View store inventory" },
            { 3, "View shopping cart" },
            { 4, "Add item to cart" },
            { 5, "Remove item from cart" },
            { 6, "Complete purchase" },
            { 7, "Exit" },
        };

        public static readonly Dictionary<int, string> CustomerMenuItems = new Dictionary<int, string>()
        {
            { 1, "New customer" },
            { 2, "Select an existing customer" }
        };

        public static List<InventoryItem> GetStoreInventory()
        {
            return new List<InventoryItem>()
            {
                 new InventoryItem("Hat","Green", 13.99M),
                 new InventoryItem("Paints", "Train", 18.55M),
                 new InventoryItem("Sun-Glasses", "Blue", 18.55M)
            };
        }
    }
}
