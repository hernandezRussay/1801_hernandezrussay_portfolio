﻿namespace HernandezRussay_CE10
{
    /// <summary>
    /// an abstract class called Logger that implements ILog
    /// </summary>
    public abstract class Logger : ILog
    {
        //logger needs a protected static int field called lineNumber
        protected static int lineNumber;//this field will track the number of lines logged which will be used when data is logged.
        //Logger does not have to provide a body for the ILog methods it should declare them as abstract so that its children have to implement them.
        public abstract void Log(string message);
        public abstract void LogD(string message);
        public abstract void LogW(string message);
    }
}
