﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Russay Hernandez
/// 4/24/2018
/// MDV2430-O
/// </summary>

namespace HernandezRussay_CE10
{
    class Program
    {
        static Dictionary<int, Action> commands;
        static Dictionary<int, Action> customerCommands;

        static Dictionary<string, Customer> customers;
        static List<InventoryItem> invetoryItems;

        static string customerName;
        static int menuItemValue = 0;

        static Logger logger;
        static Program()
        {
            customers = new Dictionary<string, Customer>();
            invetoryItems = new List<InventoryItem>();
            logger = new LogToFile();

            foreach (var item in Utils.GetStoreInventory())
            {
                logger.Log(string.Format("Creating Item: {0} - {1} with a prices of {2}",
                    item.GetStyle(), item.GetName(), item.GetPrice()));
                invetoryItems.Add(item);
            }

            

            BindCommands();
        }

        static void Main(string[] args)
        {
            while(true)
            {
                Console.Clear();

                ShowMainMenu();

                string menuItem = Console.ReadLine();

                //set to exit
                int.TryParse(menuItem, out menuItemValue);
                //start handler

                if (commands.ContainsKey(menuItemValue))
                    commands[menuItemValue].Invoke();
                else
                    commands[commands.Keys.Last()].Invoke();
            }
        }

        private static void ShowMainMenu()
        {
            Console.WriteLine("Current customer: {0}", customerName ?? "No customer selected");
            Console.WriteLine("\nWelcome to Generic Store Co");

            foreach (var menuItem in Utils.MainMenuItems)
                Console.WriteLine("{0}. {1}", menuItem.Key, menuItem.Value);
            Console.Write("What would you like to do? ");
        }
        private static void ShowCustomerMenu()
        {
            Console.WriteLine("\nChange customer");

            foreach (var menuItem in Utils.CustomerMenuItems)
                Console.WriteLine("{0}. {1}", menuItem.Key, menuItem.Value);

            Console.Write("What would you like to do? ");
        }
        private static void BindCommands()
        {
            commands = new Dictionary<int, Action>()
            {
                { 1, SelectCurrentShopper },
                { 2, ViewStoreInventory },
                { 3, ViewShoppingCart },
                { 4, AddItemToCart },
                { 5, RemoveItemToCart },
                { 6, CompletePurchase },
                { 7, Exit }
            };

            customerCommands = new Dictionary<int, Action>()
            {
                { 1, CreateCustomer },
                { 2, SelectExistingCustomer }
            };
        }
        private static bool ValidateCurrentCustomer()
        {
            if (customerName == null)
            {
                Console.WriteLine("\nPlease select a customer first.");
                Console.WriteLine("Press a key to continue.");
                Console.ReadKey();

                return false;
            }

            return true;
        }

        #region Main menu handlers
        private static void SelectCurrentShopper()
        {
            ShowCustomerMenu();

            string menuItem = Console.ReadLine();
            int.TryParse(menuItem, out menuItemValue);

            if (customerCommands.ContainsKey(menuItemValue))
                customerCommands[menuItemValue].Invoke();
            else
                customerCommands[customerCommands.Keys.Last()].Invoke();
        }
        private static void ViewStoreInventory()
        {
            if (!ValidateCurrentCustomer())
                return;

            Console.Clear();

            foreach (var item in invetoryItems)
                Console.WriteLine(item);

            Console.WriteLine("Press a key to continue.");
            Console.ReadKey();
        }
        private static void ViewShoppingCart()
        {
            if (!ValidateCurrentCustomer())
                return;

            Console.Clear();

            Customer customer = customers[customerName];

            Console.WriteLine("\n{0}'s cart contents", customerName);

            foreach (var item in customer.GetShoppingCart())
                Console.WriteLine(item);

            Console.WriteLine("\nPress a key to continue.");
            Console.ReadKey();
        }
        private static void AddItemToCart()
        {
            if (!ValidateCurrentCustomer())
                return;

            Console.Clear();

            for (int i = 0; i < invetoryItems.Count; i++)
                Console.WriteLine("{0}. {1} - {2}", i + 1, invetoryItems[i].GetStyle(), invetoryItems[i].GetName());

            Console.WriteLine("{0}. Exit", invetoryItems.Count + 1);
            Console.Write("Select an item to add: ");

            if (int.TryParse(Console.ReadLine(), out menuItemValue) && menuItemValue > 0 && menuItemValue <= invetoryItems.Count)
            {
                var item = invetoryItems[menuItemValue - 1];

                customers[customerName].AddInventoryItem(item);
                invetoryItems.Remove(item);

                 logger.Log(string.Format("Customer: {0} adding item: {1} - {2} to shopping cart", customerName, item.GetStyle(), item.GetName()));
            }

            Console.WriteLine("Press a key to continue.");
            Console.ReadKey();
        }
        private static void RemoveItemToCart()
        {
            if (!ValidateCurrentCustomer())
                return;

            Console.Clear();

            Customer customer = customers[customerName];

            Console.WriteLine("\n{0}'s shopping cart", customerName);

            var shoppingCart = customer.GetShoppingCart();
            for (int i = 0; i < shoppingCart.Count; i++)
                Console.WriteLine("{0}. {1} - {2}", i + 1, shoppingCart[i].GetStyle(), shoppingCart[i].GetName());

            Console.WriteLine("{0}. Exit", shoppingCart.Count + 1);

            Console.Write("Select an item to remove: ");
            if (int.TryParse(Console.ReadLine(), out menuItemValue) && menuItemValue > 0 && menuItemValue <= shoppingCart.Count)
            {
                var item = shoppingCart[menuItemValue - 1];

                invetoryItems.Add(item);
                customers[customerName].RemoveInventoryItem(item);

                logger.Log(string.Format("Customer: {0} removing item: {1} - {2} from shopping cart", customerName, item.GetStyle(), item.GetName()));
            }

            Console.WriteLine("Press a key to continue.");
            Console.ReadKey();
        }
        private static void CompletePurchase()
        {
            if (!ValidateCurrentCustomer())
                return;

            Console.Clear();

            Console.WriteLine("Cashing out {0}\n", customerName);
            logger.Log(string.Format("Cashing out {0}", customerName));

            foreach (var item in customers[customerName].GetShoppingCart())
            {
                Console.WriteLine(item);
                logger.Log(item.ToString());
            }


            decimal total = customers[customerName].GetShoppingCart().Sum(x => x.GetPrice());
            Console.WriteLine("Total: {0:C}", total);
            logger.Log(string.Format("Total: {0:C}", total));

            customers.Remove(customerName);
            customerName = null;

            Console.WriteLine("\nPress a key to continue.");
            Console.ReadKey();
        }
        private static void Exit()
        {
            Environment.Exit(0);
        }
        #endregion

        #region Customer menu handlers
        private static void CreateCustomer()
        {
            Console.Write("\nCustomer name? ");
            string name = Console.ReadLine();

            while (customers.ContainsKey(name))
            {
                Console.Write("That customer already exists please enter a different name: ");
                name = Console.ReadLine();
            }

            customers.Add(name, new Customer(name));
            customerName = name;

            logger.Log(string.Format("Customer: {0}", name));
            logger.Log(string.Format("Current customer changed to {0}", name));

            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }
        private static void SelectExistingCustomer()
        {
            Console.WriteLine("Current customers");

            string[] customerNames = customers.Keys.ToArray();

            for (int i = 0; i < customerNames.Length; i++)
                Console.WriteLine("{0}. {1}", i + 1, customerNames[i]);

            Console.WriteLine("{0}. Exit", customerNames.Length + 1);
            Console.Write("Select a customer: ");

            //set to exit
            menuItemValue = customerNames.Length + 1;
            if (!(int.TryParse(Console.ReadLine(), out menuItemValue) && menuItemValue > 0 && menuItemValue <= customerNames.Length))
                Console.WriteLine("\nCurrent customer not changed.");
            else
            {
                customerName = customerNames[menuItemValue - 1];
                logger.Log(string.Format("Current customer changed to {0}", customerName));
            }
               

            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }
        #endregion
    }
}
