﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hernandez_russay_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {

            //Russay Hernandez
            //December 9, 2017
            //Conditionals

            /*problem one*/
            
                        Console.WriteLine("please give me a weight"); // prompt the user for a weight
                        string weight = Console.ReadLine();             //caught the user response in a string variable

                        double user;

                        while (!double.TryParse(weight, out user))      //validated the user with a while loop, to ensure the response does not contain letters
                        {
                            Console.WriteLine("Please re enter a weight");
                            weight = Console.ReadLine();
                        }


                        Console.WriteLine("enter g for gram or oz for ounce");  //prompt the user for a g or oz. based on the responce depends what message the user will see
                        string gorOZ = Console.ReadLine();                      //stored the user response in a string variable

                        while (double.TryParse(gorOZ, out user))                //validated the users response, ensuring we didnt receive a number 
                        {
                            Console.WriteLine("Please re enter your g or oz");  //asked the useer to re enter the information
                            gorOZ = Console.ReadLine();                         //stored the users response in a variable
                        }
                        double weightCon = double.Parse(weight);                //converted weight to a double variable, something i can work with when doing operations 

                        double ounce = 28.34952;                                //stored ounce and grams in variables
                        double gram = 1.70097;

                        double inOz = ounce * weightCon;                        //multiplied ounce and gram with the user responce 
                        double inGram = gram * weightCon;


                        if (gorOZ == "oz"){                                     //if the user writes "oz" then he recieves the weight converted in ounces

                            Console.WriteLine("your weight in ounce is "+inOz+"."); 
                        }else if(gorOZ == "g"){                                     //if the user responded with a "g" he gets the weight in grams

                            Console.WriteLine("your weight in gram is "+inGram+".");
                        }
            
            /*problem two*/
            
             Console.WriteLine("How many pizzas did you order?");//prompted the user for information 4 times
             string numPizza = Console.ReadLine();                  //stored the string responce in a variable 
             double numPizza2 = double.Parse(numPizza);             //converted the string to double                

             Console.WriteLine("How many slices per pizza?");
             string numSlices = Console.ReadLine();
             double numSlices2 = double.Parse(numSlices);

             Console.WriteLine("How many guests will be at the party?");
             string numGuest = Console.ReadLine();
             double numGuest2 = double.Parse(numGuest);

             Console.WriteLine("How many slices will each guest eat?");
             string numAte = Console.ReadLine();
             double numAte2 = double.Parse(numAte);

             double totalSlices = numPizza2 * numSlices2;  //multiplied the number of pizza and number of slices to get the total number of slices
             double slicesPer = numGuest2 * numAte2;        //multiplied the number of guests to the number of eaten slices to get the slices per guests
             double slicesTo = totalSlices - slicesPer;     //subtracted the slices they ate to total slices 

             if (slicesPer < totalSlices)                   //if the slices per person is less than the total slices do this
             {
                 Console.WriteLine("you have enough pizza, with "+slicesTo+" slices left over");
             }else{                                         //if not do this 

                 Console.WriteLine("you need at least "+slicesTo*-1+" more slices. You should order more pizza");//mutiplied slices to by -1 to get a whole number 
             };
             
            /*problem 3*/
            
            Console.WriteLine("Please enter your income");          //prompted the user for his income
            string income = Console.ReadLine();                     //stored the responce in a variable
            

            double user2;
            while (!double.TryParse(income, out user2))             //validated the user to ensure i get numbers and not letters
            {
                Console.WriteLine("Please re enter your income");   //prompted the user to re enter information should he type a letter instead of a number
                 income = Console.ReadLine();
            }

            double income2 = double.Parse(income);          //converted the string into double
            double[] myArray = new double[7];               //created an array to store the tiers
            double[] myArray2 = new double[7];              //created an array to store the percentage  

            myArray[0] = 1;                 //stored the tiers in each of the index
            myArray[1] = 2;
            myArray[2] = 3;
            myArray[3] = 4;
            myArray[4] = 5;
            myArray[5] = 6;
            myArray[6] = 7;

            myArray2[0] = 10;               //stored the percentage in each of the index of my second arrays
            myArray2[1] = 15;
            myArray2[2] = 25;
            myArray2[3] = 28;
            myArray2[4] = 33;
            myArray2[5] = 35;
            myArray2[6] = 39.6;

            if (income2 < 9000) {       //created if else statements to display a response based on the users income
                Console.WriteLine("Based on your income of $" + income2 + ", you are in tier " + myArray[0] + " and your tax rate is " + myArray2[0] + "%.");
            } else if (income2 < 37950) { //each else if statement displays information based on the income, then displayed items in array
                Console.WriteLine("Based on your income of $" + income2 + ", you are in tier " + myArray[1] + " and your tax rate is " + myArray2[1] + "%.");
            } else if (income2 < 91900) {
                Console.WriteLine("Based on your income of $" + income2 + ", you are in tier " + myArray[2] + " and your tax rate is " + myArray2[2] + "%.");
            } else if (income2 < 191650) {
                Console.WriteLine("Based on your income of $" + income2 + ", you are in tier " + myArray[3] + " and your tax rate is " + myArray2[3] + "%.");
            } else if (income2 < 416700) {
                Console.WriteLine("Based on your income of $" + income2 + ", you are in tier " + myArray[4] + " and your tax rate is " + myArray2[4] + "%.");
            } else if (income2 < 418400) {
                Console.WriteLine("Based on your income of $" + income2 + ", you are in tier " + myArray[5] + " and your tax rate is " + myArray2[5] + "%.");
            } else if (income2 > 418400){
                Console.WriteLine("Based on your income of $" + income2 + ", you are in tier " + myArray[6] + " and your tax rate is " + myArray2[6] + "%.");
            }

            
            /*Problem 4*/

            Console.WriteLine("What is the cost of the first Item?"); //promted the user for cost of items then stored it in variables
            string firstItem = Console.ReadLine();
            Console.WriteLine("What is the cost of the second item?");
            string secondItem = Console.ReadLine();

            double user3;

            while (!double.TryParse(firstItem, out user3))              //validated the users responce to ensure accurate information
            {
                Console.WriteLine("Please re enter the cost of your first item"); //requested the user to re enter information if user response was wrong
                firstItem = Console.ReadLine();             //then stored it in a variable
            }
            
            while (!double.TryParse(secondItem, out user3))             //validated the second prompt
            {
                Console.WriteLine("Please re enter the item of the second item");
                secondItem = Console.ReadLine();
            }

            decimal firstItem2 = decimal.Parse(firstItem);              //converted the two responses to decimal
            decimal secondItem2 = decimal.Parse(secondItem);
            decimal totalCost = firstItem2 + secondItem2;               //added the price of both items to get a total cost, then stored it in a variable
            decimal with5 = 5 / 100m * totalCost;                       //created an operation to get the percentage of 5
            decimal with10 = 10 / 100m * totalCost;                     //created an operation to get the percentage of 10
            decimal tot1 = totalCost - with5;                            //total cost minus a 5% discount
            decimal tot2 = totalCost - with10;                          //total cost minus a 10% discount



            if (totalCost < 50) { //if total cost is less than 50 there is no discount
                Console.WriteLine("Your total cost is $" + totalCost + ".");
            } else if (totalCost < 100) {  // if total cost is more than 50 but less than 100 then they get a 5 percent discount
                Console.WriteLine("Your total cost is $" + tot1 + ".");
            } else if (totalCost > 100) {   //if total cost is more than 100 they get a 10 percent discount
                Console.WriteLine("Your total cost is $" + tot2 + ".");
            }


        }
    }
}
