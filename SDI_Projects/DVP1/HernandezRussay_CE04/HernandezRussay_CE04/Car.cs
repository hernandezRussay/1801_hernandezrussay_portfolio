﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Russay Hernandez
/// 4/20/2018
/// MDV2430-O
/// </summary>

namespace HernandezRussay_CE04
{
    public class Car
    {
        //make of type string
        private string make;
        //model of type string
        private string model;
        //color of type string
        private string color;
        //mileage of type float
        private float mileage;
        //year of type int
        private int year;
        
        /// <summary>
        /// a constructor that takes in parameters for each of its fields and initializes them
        /// </summary>
        /// <param name="make"></param>
        /// <param name="model"></param>
        /// <param name="color"></param>
        /// <param name="mileage"></param>
        /// <param name="year"></param>
        public Car(string make, string model, string color, float mileage, int year) {
           
            //The new values of the car’s fields should be logged using the LogD 
            //method of the static Logger object in the Program class. Use GetLogger().
            this.make = make;
            Program.GetLogger().LogD("Field make " + this.make+ " has been initialized.");
            this.model = model;
            Program.GetLogger().LogD("Field model " + this.model + " has been initialized.");
            this.color = color;
            Program.GetLogger().LogD("Field color " + this.color + " has been initialized.");
            this.mileage = mileage;
            Program.GetLogger().LogD("Field mileage " + this.mileage.ToString() + " has been initialized.");
            this.year = year;
            Program.GetLogger().LogD("Field year " + this.year.ToString() + " has been initialized.");
        }
        /// <summary>
        /// a Drive method that returns nothing and takes a parameter of type float
        /// The value passed in should be used to update the car’s mileage.
        /// The new mileage should be logged using the LogW method of the static Logger
        /// object in the Program class. Use GetLogger().
        /// </summary>
        /// <param name="mileage"></param>
        public void Drive(float mileage) {
            this.mileage += mileage;
            Program.GetLogger().LogW("The car's mileage has been updated to " + this.mileage.ToString() + ".");
        }
    }
}
