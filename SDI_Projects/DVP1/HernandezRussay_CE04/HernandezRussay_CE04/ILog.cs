﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Russay Hernandez
/// 4/20/2018
/// MDV2430-O
/// </summary>

namespace HernandezRussay_CE04
{
    public interface ILog
    {
        /// <summary>
        /// returns nothing and takes a string parameter
        /// </summary>
        /// <param name="message"></param>
        void Log(string message);
        /// <summary>
        /// LogD - returns nothing and takes a string parameter
        /// </summary>
        /// <param name="message"></param>
        void LogD(string message);
        /// <summary>
        /// LogW - returns nothing and takes a string parameter
        /// </summary>
        /// <param name="message"></param>
        void LogW(string message);
    }
}
