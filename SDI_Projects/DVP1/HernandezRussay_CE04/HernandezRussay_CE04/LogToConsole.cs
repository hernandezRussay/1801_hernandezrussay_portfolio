﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Russay Hernandez
/// 4/20/2018
/// MDV2430-O
/// </summary>

namespace HernandezRussay_CE04
{
    /// <summary>
    /// a class called LogToConsole that inherits from Logger
    /// </summary>
    public class LogToConsole : Logger
    {
        /// <summary>
        /// Log - should use Console.Writeline to output the lineNumber and the string that was passed in. It should also increase the lineNumber.
        /// </summary>
        /// <param name="message"></param>
        public override void Log(string message)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("{0} - {1}",lineNumber,message);
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            lineNumber++;
        }
        /// <summary>
        /// LogD - should use Console.Writeline to output the lineNumber, the word DEBUG,
        /// and the string that was passed in. It should also increase the lineNumber.
        /// </summary>
        /// <param name="message"></param>
        public override void LogD(string message)
        {
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0}. {1} - {2}", lineNumber, "DEBUG", message);
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            lineNumber++;
        }
        /// <summary>
        /// LogW - should use Console.Writeline to output the lineNumber, the word WARNING,
        /// and the string that was passed in. It should also increase the lineNumber.
        /// </summary>
        /// <param name="message"></param>
        public override void LogW(string message)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("{0}. {1} - {2}", lineNumber, "WARNING", message);
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            lineNumber++;
        }
    }
}
