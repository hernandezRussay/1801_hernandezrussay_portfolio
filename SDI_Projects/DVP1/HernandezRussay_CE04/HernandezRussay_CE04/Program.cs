﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Russay Hernandez
/// 4/20/2018
/// MDV2430-O
/// </summary>

namespace HernandezRussay_CE04
{
    public class Program
    {
        

        //a private static field of type Logger
        private static Logger logger = null;//this field should be initialized to null
        /// <summary>
        /// Main method
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args) {
            //Car variable to use for the currentCar.
            Car currentCar = null;
            string command = "";
            string[] menuItems = { "Disable logging", "Enable logging", "Create a car", "Drive the car", "Destroy the car", "Exit" };
            bool closeProgram = false;
            do{
                for (int i = 0; i < 6; i++) {
                    Console.WriteLine("{0}. {1}", (i + 1), menuItems[i]);
                }
                bool isCorrect = false;
                Console.Write("> ");
                command = Console.ReadLine();

                if (command.CompareTo("1") == 0 || command.ToUpper().CompareTo(menuItems[0].ToUpper()) == 0)
                {
                    //create a new DoNotLog object and store it in Program’s static Logger field.
                    logger = new DoNotLog();
                    Console.WriteLine("\nLogging is disable.\n");
                }
                else if (command.CompareTo("2") == 0 || command.ToUpper().CompareTo(menuItems[1].ToUpper()) == 0)
                {
                    //create a new LogToConsole object and store it in Program’s static Logger field. 
                    logger = new LogToConsole();
                    Console.WriteLine("\nLogging is enable.\n");
                }
                else if (command.CompareTo("3") == 0 || command.ToUpper().CompareTo(menuItems[2].ToUpper()) == 0)
                {
                    if (logger==null) {
                        logger = new DoNotLog();
                    }
                    //prompt the user for values to create a car object and store the object in currentCar.
                    string make = "";
                    while (!isCorrect)
                    {
                        Console.Write("Enter the make of car: ");
                        make = Console.ReadLine();
                        if (make.CompareTo("") == 0)
                        {
                            Console.WriteLine("\nEnter correct the make of car!\n");
                        }
                        else {
                            isCorrect = true;
                        }
                    }
                    string model = "";
                    isCorrect = false;
                    while (!isCorrect)
                    {
                        Console.Write("Enter the model of car: ");
                        model = Console.ReadLine();
                        if (model.CompareTo("") == 0)
                        {
                            Console.WriteLine("\nEnter correct the model of car!\n");
                        }
                        else {
                            isCorrect = true;
                        }
                    }
                    string color = "";
                    isCorrect = false;
                    while (!isCorrect)
                    {
                        Console.Write("Enter the color of car: ");
                        color = Console.ReadLine();
                        if (color.CompareTo("") == 0)
                        {
                            Console.WriteLine("\nEnter correct the color of car!\n");
                        }
                        else
                        {
                            isCorrect = true;
                        }
                    }
                    float mileage = -1;
                    isCorrect = false;
                    while (!isCorrect){
                        Console.Write("Enter the mileage of car: ");
                        if (!float.TryParse(Console.ReadLine(), out mileage) || mileage <= 0)
                        {
                            mileage = -1;
                            Console.WriteLine("\nEnter correct mileage as float value >0!\n");
                        }
                        else
                        {
                            isCorrect = true;
                        }
                    }
                    int year = -1;
                    isCorrect = false;
                    while (!isCorrect)
                    {
                        Console.Write("Enter the year of car [1900-2018]: ");
                        if (!int.TryParse(Console.ReadLine(), out year) || year < 1900 || year > 2018)
                        {
                            year = -1;
                            Console.WriteLine("\nEnter correct year as integer value [1900-2018]!\n");
                        }
                        else {
                            isCorrect = true;
                        }
                    }
                    Console.WriteLine("");
                    currentCar = new Car(make, model, color, mileage, year);
                    Console.WriteLine("\nNew car has been created.\n");
                }
                else if (command.CompareTo("4") == 0 || command.ToUpper().CompareTo(menuItems[3].ToUpper()) == 0)
                {
                    //prompt the user for how far they are driving the car and call the Drive method on currentCar.
                    if (currentCar != null)
                    {
                        float mileage = -1;
                        isCorrect = false;
                        while (!isCorrect) {
                            Console.Write("How far are you driving the car?: ");
                            if (!float.TryParse(Console.ReadLine(), out mileage) || mileage <= 0)
                            {
                                mileage = -1;
                                Console.WriteLine("\nEnter correct mileage as float value >0!\n");
                            }
                            else {
                                isCorrect = true;
                            }
                        }
                        currentCar.Drive(mileage);
                    }
                    else {
                        Console.WriteLine("\nError: create a car object!\n");
                    }
                }
                else if (command.CompareTo("5") == 0 || command.ToUpper().CompareTo(menuItems[4].ToUpper()) == 0)
                {
                    //set currentCar to null.
                    currentCar = null;
                    Console.WriteLine("\nThe car has been destroyed.\n");
                }
                else if (command.CompareTo("6") == 0 || command.ToUpper().CompareTo(menuItems[5].ToUpper()) == 0)
                {
                    closeProgram = true;
                }
                else {
                    Console.WriteLine("\nError: enter correct command!\n");
                }
                //stop the program
            } while (!closeProgram);
        }
        /// <summary>
        /// public static method GetLogger that returns the value held by the above field.
        /// </summary>
        /// <returns></returns>
        public static Logger GetLogger() {
            return logger;
        }
    }
}
