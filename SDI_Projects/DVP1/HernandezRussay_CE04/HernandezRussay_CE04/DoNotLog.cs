﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Russay Hernandez
/// 4/20/2018
/// MDV2430-O
/// </summary>

namespace HernandezRussay_CE04
{
    /// <summary>
    /// a class called DoNotLog that inherits from Logger
    /// </summary>
    public class DoNotLog : Logger
    {
        /// <summary>
        /// Log - should do nothing
        /// </summary>
        /// <param name="message"></param>
        public override void Log(string message){}
        /// <summary>
        /// LogD - should do nothing
        /// </summary>
        /// <param name="message"></param>
        public override void LogD(string message) { }
        /// <summary>
        /// LogW - should do nothing
        /// </summary>
        /// <param name="message"></param>
        public override void LogW(string message) { }
    }
}
